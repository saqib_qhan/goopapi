<? if ($message) : ?>
    <h3 class="message">
        <?= $message; ?>
    </h3>
<? endif; ?>

<div id="brandlarge">
    <h1>Goop. IOS App Tester</h1>

    <p></p>
    <? if ($tester) : ?>
        <p> Sign In as a Tester</p>
    <? endif; ?>

</div>

<div class="modalcontainer thinbase login">
    <div class="modalcontainer-header">
        <h3>Login</h3></div>
    <div class="modalcontainer-body">
        <?= Form::open(); ?>
        <div style="display: none">
            <?= Form::hidden('team_token', $team_token) ?>
        </div>
        <div class="clearfix">
            <?= Form::label('username', 'Email'); ?>
            <?= Form::input('username', HTML::chars(Arr::get($_POST, 'username'))); ?>
        </div>
        <div class="clearfix">
            <?= Form::label('password', 'Password'); ?>
            <?= Form::password('password'); ?>
        </div>

        <div class="clearfix">
            <?= Form::label('remember', 'Remember Me'); ?>
            <?= Form::checkbox('remember'); ?>
        </div>

        <div class="pull-right">
            <?= Form::submit('login', 'Login'); ?>
        </div>

        <div class="linkalign">
            <? if ($tester): ?>
                <? if ($team): ?>
                    <?= HTML::anchor("/registration/sign_up/tester", "Don't have an account?"); ?>
                <? else: ?>
                    <?= HTML::anchor("/registration/sign_up/tester", "Don't have an account?"); ?>
                <? endif ?>
            <? else: ?>
                <?= HTML::anchor("/registration/sign_up", "Don't have an account?"); ?>
            <? endif ?>
        </div>
        <?= Form::close(); ?>

    </div>
    <!-- /modalcontainer-body -->
</div>


