<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <meta name="description" content="Goop IOS App Tester"/>
    <meta name="author" content="GoopDev"/>
    <meta name="copyright" content="Goop"/>
    <meta name="language" content="en-us"/>
    <title>Goop IOS App Tester</title>
    <?php echo HTML::style("media/css/base.css"); ?>
    <?php echo HTML::style("media/css/bootstrap.css"); ?>
    <?php echo HTML::style("media/css/bootstrap-responsive.css"); ?>
    <?php echo HTML::style("media/css/bootstrap-theme.css"); ?>
    <?php echo HTML::style("media/css/jquery-ui.css"); ?>


    <?php echo HTML::script("media/js/jquery-1.11.0.min.js"); ?>
    <?php echo HTML::script("media/js/jquery-migrate-1.2.1.min.js"); ?>
    <?php echo HTML::script("media/js/jquery-ui.js"); ?>
    <?php echo HTML::script("media/js/base.js"); ?>
    <?php echo HTML::script("media/js/bootstrap.js"); ?>



    <style type="text/css">
        .error {
            color: red;
        }

        .message {
            padding: 10px;
            background-color: yellow;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="content nohead grid">

        <div class="container">
            <div class="content vert-navbg ">
                <nav class="vert-nav">
                    <ul>
                        <li class="anchor"><h1>
                                <?= html::image('media/img/android-icon.png', array('alt' => 'Thumbnail', 'class' => 'icon', 'width' => '64px', '64px')); ?>
                                <!--                                <img class="icon" src="/media/img/android-icon.png" width="64px" height="64px">-->
                                <i class="app-type-ios"></i></h1>

                            <h2><?= $app->name ?></h2>
                            Build <?= $build->version_name ?> (<?= $build->version_code ?>)

                        </li>
                        <li class="navheader"><a>Settings</a></li>
                        <li>
                            <?= html::anchor("/build/edit/{$build->id}/", 'Build Information', array('class' => 'settings active')); ?>
                        </li>
                        <li>
                            <?= html::anchor("/build/permissions/{$build->id}", 'Permissions', array('class' => 'testers')); ?>
                        </li>
                        <li class="navheader">
                            <?= html::anchor('#', 'Reports'); ?>
                        </li>
                        <li>
                            <?= html::anchor("/build/activity/{$build->id}/", 'Activity', array('class' => 'report')); ?>
                        </li>
                        <li>
                            <a class="feedback" href="#"><span><?= $build->installs ?></span>Feedback</a>
                        </li>
                        <? if ($app->sdk == '') : ?>
                            <li><h5>iOS SDK Features</h5>

                                <p>Install the TestFlight SDK to unlock these features and more!
                                    <?= html::anchor('#', 'Get the SDK', array('class' => 'getsdk')); ?>
                                </p>
                            </li>

                            <li>
                                <?= html::anchor('#', 'Sessions', array('class' => 'sessions inactive')); ?>
                            </li>
                            <li>
                                <?= html::anchor('#', 'Crashes', array('class' => 'crashes inactive')); ?>
                            </li>
                            <li>
                                <?= html::anchor('#', 'Checkpoints', array('class' => 'checkpoints inactive')); ?>
                            </li>
                            <? else : ?>
                            <li>
                                <?= html::anchor('#', 'Sessions', array('class' => 'sessions')); ?>
                            </li>
                            <li>
                                <?= html::anchor('#', 'Crashes', array('class' => 'crashes')); ?>
                            </li>
                            <li>
                                <?= html::anchor('#', 'Checkpoints', array('class' => 'checkpoints')); ?>
                            </li>

                        <? endif; ?>
                    </ul>
                </nav>

                <?= $content; ?>

            </div>
        </div>

    </div>
</div>

<footer class="container">
</footer>
</body>
</html>
