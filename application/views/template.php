<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head> 
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" /> 
<meta name="description" content="Goop IOS App Tester" /> 
<meta name="author" content="GoopDev" /> 
<meta name="copyright" content="Goop" />
<meta name="language" content="en-us" /> 
<title>Goop IOS App Tester</title> 
<?php echo HTML::style("media/css/base.css"); ?>
<?php echo HTML::style("media/css/bootstrap.css"); ?>
<?php echo HTML::style("media/css/bootstrap-responsive.css"); ?>
<?php echo HTML::style("media/css/bootstrap-theme.css"); ?>
<?php echo HTML::style("media/css/jquery-ui.css"); ?>


<?php echo HTML::script("media/js/jquery-1.11.0.min.js"); ?>
<?php echo HTML::script("media/js/jquery-migrate-1.2.1.min.js"); ?>
<?php echo HTML::script("media/js/jquery-ui.js"); ?>
<?php echo HTML::script("media/js/base.js"); ?>
<?php echo HTML::script("media/js/bootstrap.js"); ?>



<style type="text/css">
.error {
	color: red;
}
.message {
	padding: 10px;
	background-color: yellow;
}
</style>
</head> 
<body>
	<div class="container-fluid">
           <div class="content nohead grid">
		<?= $content; ?>
	  </div>
        </div>

<footer class="container">
</footer>
</body>
</html>
