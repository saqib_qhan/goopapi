<? if ($message) : ?>
    <h3 class="message">
        <?= $message; ?>
    </h3>
<? endif; ?>

<div id="brandlarge"><h1>Goop.App Tester</h1>

    <p></p>
    <? if ($tester) : ?>
        <p> Join as a Tester</p>
    <? endif; ?>
</div>

<div class="modalcontainer thinbase login header-signup">
    <div class="modalcontainer-header">
        <h3>Sign Up</h3></div>
    <div class="modalcontainer-body">
        <?= Form::open(); ?>
        <div style="display: none">
            <?= Form::hidden('tester', $tester) ?>
            <? if ($team): ?>
                <?= Form::hidden('team', $team->id) ?>
            <? endif; ?>
        </div>
        <div class="clearfix">
            <?= Form::label('username', 'Username'); ?>
            <?= Form::input('username', HTML::chars(Arr::get($_POST, 'username'))); ?>
            <div class="error">
                <?= Arr::get($errors, 'username'); ?>
            </div>

        </div>

        <div class="clearfix">
            <?= Form::label('email', 'Email Address'); ?>
            <?= Form::input('email', HTML::chars(Arr::get($_POST, 'email'))); ?>
            <div class="error">
                <?= Arr::get($errors, 'email'); ?>
            </div>
        </div>

        <div class="clearfix">
            <?= Form::label('password', 'Password'); ?>
            <?= Form::password('password'); ?>
            <div class="error">
                <?= Arr::path($errors, '_external.password'); ?>
            </div>
        </div>

        <div class="clearfix">
            <?= Form::label('password_confirm', 'Confirm Password'); ?>
            <?= Form::password('password_confirm'); ?>
            <div class="error">
                <?= Arr::path($errors, '_external.password_confirm'); ?>
            </div>
        </div>

        <div class="clearfixSlider">
            <?= Form::label('developer', 'Developer'); ?>
            <div class="slider-outer">
                <?= Form::hidden('developer', 0); ?>
                <div class="slider"></div>
            </div>
        </div>

        <div class="containerSignUp">
            <div class="pull-right pull-rightSignUp">
                <?= Form::submit('create', 'Sign Up'); ?>
                <?= Form::close(); ?>
            </div>

            <div class="linkalign">
                <? if ($tester): ?>
                    <? if ($team): ?>
                        <?= HTML::anchor("/registration/sign_in/tester", 'Already Signed Up?'); ?>
                    <? else: ?>
                        <?= HTML::anchor("/registration/sign_in/tester", 'Already Signed Up?'); ?>
                    <? endif ?>
                <? else: ?>
                    <?= HTML::anchor("/registration/sign_in", 'Already Signed Up?'); ?>
                <? endif ?>
            </div>
        </div>

    </div>
</div>


