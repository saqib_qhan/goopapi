<? if ($message) : ?>
    <h3 class="message">
        <?= $message; ?>
    </h3>
<? endif; ?>
<?php echo HTML::script("media/js/goop.js"); ?>
<?php echo HTML::script("media/js/fileuploader.js"); ?>

<?php echo HTML::style("media/css/ipa.css"); ?>
<?php echo HTML::style("media/css/build_tmplte.css"); ?>
<?php echo HTML::script("media/js/ipa.js"); ?>

<div class="main ">
    <section class="contain"><h1 class="header-appinfo">
            Build Information
        </h1>
        <hr>
        <div class="row">
            <div class="span14">
                <div class="flexrow" style="padding-left:10px"><label class="column four">
                        General
                        <br>
                        <small>Associated metadata for this build</small>
                    </label>

                    <div class="column eight">
                        <table>
                            <tbody>
                            <tr>
                                <td><strong>Released</strong></td>
                                <td><span class="localtime"><?= $build->created_at ?></span></td>
                            </tr>
                            <!--                            <tr>-->
                            <!--                                <td><strong>Built for</strong></td>-->
                            <!--                                <td>Universal iOS 4.3+</td>-->
                            <!--                            </tr>-->
                            <!--                            <tr>-->
                            <!--                                <td><strong>Uploaded From</strong></td>-->
                            <!--                                <td>Dashboard</td>-->
                            <!--                            </tr>-->
                            <tr>
                                <td><strong>Size</strong></td>
                                <td><?= $build->file_size / 1000000 ?> MB</td>
                            </tr>
                            <!--                            <tr>-->
                            <!--                                <td><strong>Type</strong></td>-->
                            <!--                                <td>Beta</td>-->
                            <!--                            </tr>-->
                            <!--                            <tr>-->
                            <!--                                <td><strong>SDK</strong></td>-->
                            <!--                                <td><a class="status-no golink"-->
                            <!--                                       href="/dashboard/applications/1103449/token/"></a><a class="golink"-->
                            <!--                                                                                            href="/dashboard/applications/1103449/token/">Implement-->
                            <!--                                        the SDK</a></td>-->
                            <!--                            </tr>-->
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <?= Form::open(); ?>
        <div style="display:none">
            <?= Form::hidden('csrf', Security::token()); ?>
            <?= Form::hidden('date', $build->updated_at); ?>
        </div>
        <fieldset>
            <div class="row">
                <div class="span14">
                    <div class="flexrow clearfix" id="releasenotes"><label class="column four">
                            Release Notes
                            <br>
                            <small>What's new with this build?</small>
                        </label>

                        <div class="column eight">
                            <? $attr = array('rows' => '10', 'name' => 'description', 'id' => 'description', 'class' => 'flex97'); ?>
                            <?= FORM::textarea("description", $build->release_notes, $attr, TRUE); ?>
                        </div>
                    </div>
                    <div class="flexrow" style="padding-left:10px"><p class="actions right">
                            <?= Form::submit('save', 'Save »', array('class' => 'primary btn wide')); ?>
                        </p></div>
                </div>
            </div>
        </fieldset>
        <?= Form::close(); ?>
        <div class="row">
            <div class="span14 alert-message block-message">
                <div class=""><h5>Remove Build</h5>

                    <div class="flexrow">
                        <div class="column seven"><span class="label warning">Warning</span> clicking the remove
                            button will delete this build <em class="red">completely</em>!
                            There is no way to undo this, so proceed with caution (here be dragons).
                        </div>
                        <div class="column five">
                            <?= HTML::anchor("/build/delete/{$build->id}", "Remove Build", array('class' => 'primary btn delete wide pull-right small')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $("input[type='submit']").click(function () {
        var dt = new Date();
        var time = dt.getFullYear() + ":" + parseInt(dt.getMonth() + 1) + ":" + dt.getDate() + ' ' + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        $("input[name='date']").val(time);
    });
</script>