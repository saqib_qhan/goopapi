<? if ($message) : ?>
    <h3 class="message">
        <?= $message; ?>
    </h3>
<? endif; ?>
<?php echo HTML::script("media/js/goop.js"); ?>

<?php echo HTML::style("media/css/ipa.css"); ?>
<?php echo HTML::style("media/css/build_tmplte.css"); ?>
<?php echo HTML::style("media/css/build_activity.css"); ?>

<div class="main ">
    <section class="contain">
        <h1 class="header-testers subhead">
            Permissions
            <div>
                <small>
                    Choose who can access and install this build. Select teammates individually or via distribution
                    lists.
                </small>
            </div>
        </h1>

        <div class="alert-message block-message error hide" id="memberlist-error"></div>
        <?= Form::open() ?>
        <!--<input type="hidden" name="new_form" value="3">-->
        <?= Form::hidden('new_form', null) ?>
        <?= Form::hidden('id', $build->id) ?>

        <div style="display:none">
            <?= Form::hidden('csrfmiddlewaretoken', Security::token()) ?>
        </div>

        <h3 class="box">
            Update Provisioning Profile
        </h3>

        <? if ($teammates_available) : ?>
            <div class="flexrow" id="nonteammate-div" style="">
                <div class="column twelve"><h3 class="box" id="nonteammate-label">
                        <span>Testers</span><span
                            title="These people are registered Goop users in your provisioning profile<br> that are not part of your team yet. Granting permission will send an<br> invite to your team"
                            rel="twipsy" class="help">help</span></h3>
                    <table class="zebra-striped" id="nonteammate-list">
                        <thead>
                        <tr>
                            <th width="30">
<!--                                <input type="checkbox" name="nonteammate_all" class="checkbox_select_all" value="s">-->
                            </th>
                            <th class="nopadleft"><strong>User</strong></th>
                            <th width="110" class="center"><span class="device-sm"><strong>Devices</strong></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($testers as $tester) : ?>
                            <tr class="pointer">
                                <td><?= Form::checkbox("testers[{$tester->id}]", $tester->id, $tester->permitted  ? true: false, array('id' => "tester_{$tester->id}", 'class' => 'checkbox usercheckbox')) ?></td>
                                <td class="nopadleft">
                                    <div class="avatar small">
                                        <?= html::image('media/img/icon-pilot.png', array('data-original' => 'media/img/icon-pilot.png', 'class' => 'lazy')); ?>
                                        <? $test_user = ORM::factory('user')->where('email', '=', $tester->email)->find() ?>
                                        <?= $test_user->username ?>
                                    </div>
                                </td>

                                <td>Number of devices
                                </td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <? else : ?>
            <div class="flexrow" id="teammate-div" style="display:none;">
                <div class="column twelve"><h3 class="box" id="teammate-label">
                        <span>Teammates In The Provisioning Profile</span><span
                            class="help" rel="twipsy"
                            title="These people are in your provisioning profile and part of your team">
                  help</span></h3>

                    <div class="distrotags">
                        <ul class="tags">
                            <li><a href="/goopiosapptest/team/member" class="createnew"><strong>Create New
                                        List</strong></a></li>
                        </ul>
                    </div>
                    <table class="zebra-striped" id="teammate-list">
                        <thead>
                        <tr>
                            <th width="30"><input type="checkbox" name="teammate_all" class="checkbox_select_all"
                                                  value="s"></th>
                            <th class="nopadleft">User</th>
                            <th class="center" width="110"><span class="device-sm">Devices</span></th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        <?endif; ?>

        <p class="actions right">
            <?= Form::submit('update', 'Update »', array('class' => 'primary btn wide pull-right', 'id' => 'uploadbtn')); ?>
        </p>
        <? Form::close(); ?>
    </section>
</div>

<script type="text/javascript">
    $("input[type='checkbox']").click(function () {
        var dt = new Date();
        var time = dt.getFullYear() + ":" + parseInt(dt.getMonth() + 1) + ":" + dt.getDate() + ' ' + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        $("input[name='date']").val(time);
    });
</script>