<? if ($message) : ?>
    <h3 class="message">
        <?= $message; ?>
    </h3>
<? endif; ?>
<?php echo HTML::script("media/js/goop.js"); ?>

<?php echo HTML::style("media/css/ipa.css"); ?>
<?php echo HTML::style("media/css/build_tmplte.css"); ?>
<?php echo HTML::style("media/css/build_activity.css"); ?>

<div class="main ">
    <section class=""><h1 class="header-reports">Activity</h1>
        <? if ($activity) : ?>
            <div class="flexrow build-report">
                <div class="column nine"><h2 class="box">Tester Status</h2>
                    <table class="zebra-striped" id="statustable">
                        <thead>
                        <tr>
                            <th class="header sortable" data-name="user">User</th>
                            <th class="header sortable" data-name="device">Device</th>
                            <th class="header sortable" data-name="latest_version">Currently Installed</th>
                            <th class="header sortable" data-name="updated">Updated</th>
                            <th class="header sortable" data-name="status">Status</th>
                        </tr>
                        </thead>
                        <tbody id="statuses">
                        <? foreach ($testers as $tester) : ?>
                            <tr>
                                <td>
                                    <div class="avatar small">

                                        <img
                                            data-original="https://secure.gravatar.com/avatar/d225c9c1cb64b452b5ad10b34fed2ecc?d=https%3A%2F%2Ftestflightapp.com%2Fmedia%2Fimg%2Ficon-pilot-42.png"
                                            class="lazy"
                                            src="https://secure.gravatar.com/avatar/d225c9c1cb64b452b5ad10b34fed2ecc?d=https%3A%2F%2Ftestflightapp.com%2Fmedia%2Fimg%2Ficon-pilot-42.png"
                                            width="28" height="28" style="display: inline;">


                                        <? $test_user = ORM::factory('user')->where('email', '=', $tester->email)->find() ?>
                                        <?= html::anchor("mailto:{$tester->email}?subject={$app->name} {$build->version_name} ({$build->version_code})", "{$test_user->username}", array('class' => 'sessions inactive', 'rel' => 'twipsy', 'data-original-title' => $tester->email)); ?>

                                    </div>

                                </td>
                                <td>

                                    <div class="flexcontain100" rel="twipsy" title="<?= $app->app_token ?>">
                                        iPad 1 (iOS 5.1.1)
                                    </div>

                                </td>
                                <td>
                                    <? $tester_emailbuild = ORM::factory('emailbuild')->where('build_id', '=', $build->id)->and_where('email', '=', $tester->email)->find() ?>
                                    <? if ($tester_emailbuild->installed) : ?>
                                        <?= $build->version_name ?> (<?= $build->version_code ?>)
                                    <? else : ?>
                                        -
                                    <? endif; ?>

                                </td>
                                <td>


                                </td>
                                <td class="status-permitted">
                                    <? if ($tester->permitted) : ?>
                                        Permitted
                                    <? else : ?>
                                        Not Permitted
                                    <? endif; ?>
                                </td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                </div>

                <div class="column three"><h2 class="box">Activity</h2>
                    <ul class="activity" id="activity" style="display: block;">
                        <li class="created">
                            <div class="avatar small">

                                <img
                                    src="https://secure.gravatar.com/avatar/5bed1199eee1ee353aa496d31f443921?d=https%3A%2F%2Ftestflightapp.com%2Fmedia%2Fimg%2Ficon-pilot-42.png"
                                    width="23" height="23">

                            </div>
                            <h5>
                                <a href="mailto:railshub@gmail.com">
                                    <?= $app->user->username ?>
                                </a><br>
                                <small>
                                    uploaded this build<br>
                                    <strong><?= $build->created_at ?></strong>
                                </small>
                            </h5>
                        </li>
                    </ul>
                    <button type="button" class="load-more" style="display: none;">Load More</button>
                </div>
            </div>
        <? else : ?>
            <div class="alert-message block-message info notice empty no-activity"><h2 class="subhead">No Activity
                    Yet</h2>
                Activity will appear when you have granted access to this build. Modify build permissions to give
                testers
                access.
                <br><br>
                <?= html::anchor("/build/permissions/{$build->id}", 'Grant Build Access', array('class' => 'btn')); ?>
            </div>
        <?endif; ?>
    </section>
</div>