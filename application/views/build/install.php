<?php echo HTML::style("media/css/ipa.css"); ?>
<?php echo HTML::style("media/css/install.css"); ?>
<div class="main">
    <section>
        <h1 class="header-buildicon subhead">
            <?= HTML::image('/media/img/android-icon.png', array('width' => '64px', 'height' => '64px')) ?>
            Install <?= $app->name ?> <?= $build->version_name ?> (<?= $build->version_code ?>)
            <div class="no-device-alert" style="display: none">
                <small>
                    To install this beta, open this page on your Android Device.
                </small>
            </div>
        </h1>
        <hr>

        <div class="yes-device-alert" style="display: none">
            <div class="row">

                <?= Form::open('build/install', array('id' => 'install_form')); ?>
                <?= Form::hidden('csrf', Security::token()); ?>
                <?= Form::hidden('id', $emailbuild->token); ?>
                <?= Form::submit('install', 'Install »', array('class' => 'primary btn wide pull-right', 'id' => 'uploadbtn')); ?>
                <?= FORM::close(); ?>
            </div>
        </div>

        <div class="row">
            <div class="span13 offset1">
                <?= Form::open('build/send_email', array('id' => 'email_form')); ?>
                <div class="yes-device-alert" style="display: none">
                    <!--                    <= Form::submit('install', 'Install »', array('class' => 'primary btn wide pull-right', 'id' => 'uploadbtn')); >-->
                </div>
                <div style="display:none">
                    <?= Form::hidden('csrf', Security::token()); ?>
                </div>
                <fieldset>
                    <div class="clearfix row callout">
                        <label for="devices" class="span11">
                            Release Notes<br>
                            <small><?= $build->release_notes ?></small>
                        </label></div>
                    <!-- /clearfix -->
                    <div class="clearfix row callout"><label for="devices" class="span11">
                            Details<br>
                            <small>
                                <p class="spacetop"><strong>Released:</strong> &nbsp;
                                    <small class="localtime"><?= $build->created_at ?></small>
                                </p>
                                <!--                                    <p><strong>Expires:</strong> &nbsp;-->
                                <!--                                        <small>10 months, 1 week</small>-->
                                <!--                                    </p>-->
                                <p><strong>Size:</strong> &nbsp;
                                    <small><?= $build->file_size / 1000000 ?> MB</small>
                                </p>
                            </small>
                        </label></div>
                    <!-- /clearfix -->
                    <div class="alert-message block-message info row no-device-alert" id="email_default"
                         style="display: none">


                        To install this beta, open this page on your iPhone, iPad or iPod touch.


                    </div>
                    <div class="alert-message block-message" id="email_sending" style="display:none">
                        Sending reminder...
                    </div>
                    <div class="alert-message block-message info" id="email_sent" style="display:none">
                        A reminder has been sent!
                    </div>
                    <!--                        <div class="clearfix" id="email_widget"><label for="email">-->
                    <!--                                Email<br>-->
                    <!--                                <small>Need us to e-mail you the link?</small>-->
                    <!--                            </label>-->
                    <!---->
                    <!--                            <div class="input">-->
                    <!--                                <input type="text" size="30" name="email" id="email" class="span9"-->
                    <!--                                                      value="railshub@gmail.com"></div>-->
                    <!--                        </div>-->
                    <!-- /clearfix -->
                    <!--                        <div class="actions right" id="sendbtn"><a href="#" class="primary btn wide" id="send"-->
                    <!--                                                                   onclick="send();">Send »</a></div>-->
                </fieldset>
                <?= FORM::close() ?>
            </div>
        </div>
        <!--/row--></section>
</div>
