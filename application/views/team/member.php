<? if ($message) : ?>
    <h3 class="message">
        <?= $message; ?>
    </h3>
<? endif; ?>
<?php echo HTML::script("media/js/goop.js"); ?>

<?php echo HTML::style("media/css/base.css"); ?>
<?php echo HTML::style("media/css/team.css"); ?>


<div class="container">
    <div class="content">
        <div class="main">
            <section>
                <h1 class="header-testers subhead">

                    Invite a teammate
                    <div>
                        <small>Invite developers and testers to your team via email.</small>
                    </div>
                </h1>
                <hr>

                <?= Form::open() ?>
                <div style="display:none">
                    <?= Form::hidden('csrf', Security::token()); ?>
                    <?= Form::hidden('team_id', Kohana::$config->load('team')->get('options')["team_id"]); ?>
                </div>
                <div class="row">
                    <div class="span13 offset1">
                        <fieldset>
                            <div class="clearfix alert-message block-message info">
                                <!--                                <div class="row"><label class="span11" for="developer">-->
                                <!--                                        Recruit via Share Link<br>-->
                                <!--                                        <small>Sending email invites is tedious. Let your teammates do the work by-->
                                <!--                                            simply sharing your recruitment URL. <a-->
                                <!--                                                href="/dashboard/team/recruitment/edit">Start recruiting now »</a>-->
                                <!--                                        </small>-->
                                <!--                                    </label>-->
                                <!--                                </div>-->
                            </div>
                            <!-- Email -->
                            <div class="clearfix ">
                                <label for="email">
                                    Email Address<span class="required">*</span><br>
                                    <small>Your teammate will be invited via email. Use comma delimited list for
                                        multiple emails.
                                    </small>
                                </label>

                                <div class="input">
                                    <?= FORM::input('email', null, array('size' => '30', 'id' => 'email', 'class' => 'span9')) ?>
                                </div>
                            </div>
                            <!-- /clearfix --><!-- Message -->
                            <div class="clearfix"><label for="id_message">
                                    Message<br>
                                    <small>Write an optional introduction message for your teammate.</small>
                                </label>

                                <div class="input">
                                    <? $attr = array('rows' => '10', 'name' => 'message', 'id' => 'id_message', 'class' => 'span9'); ?>
                                    <?= FORM::textarea("message", '', $attr, TRUE); ?>
                                </div>
                            </div>
                            <!-- /clearfix --><!-- Checkbox -->
                            <div class="clearfix row callout"><span class="checkbox">
<!--                                    <div-->
<!--                                        class="iPhoneCheckContainer" style="width: 63px;">-->
<!--                                        <input type="checkbox"-->
<!--                                                                                                 id="id_developer"-->
<!--                                                                                                 name="developer"><label-->
<!--                                            class="iPhoneCheckLabelOff" style="width: 74px;">-->
<!--                                            <span>OFF</span>-->
<!--                                        </label><label class="iPhoneCheckLabelOn" style="width: 0px;">-->
<!--                                            <span style="margin-left: -39px;">ON</span>-->
<!--                                        </label>-->
<!---->
<!--                                        <div class="iPhoneCheckHandle" style="width: 34px;">-->
<!--                                            <div class="iPhoneCheckHandleRight">-->
<!--                                                <div class="iPhoneCheckHandleCenter"></div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
                                    <div class="slider-outer">
                                        <?= Form::hidden('developer', 0); ?>
                                        <div class="slider"></div>
                                    </div>


                                </span><label for="developer" class="span11">
                                    Team Developer?<br>
                                    <small>Developers can help manage this team by uploading builds and inviting
                                        teammates. Both developers and testers can install and test builds
                                    </small>
                                </label></div>
                            <!-- /clearfix --><!-- If profile attached --><p class="actions right">
                                <?= Form::submit('save', 'Send Invitation »', array('class' => 'primary btn wide pull-right', 'id' => 'uploadbtn')); ?>
                            </p></fieldset>
                    </div>
                </div>
                <?= Form::close() ?>
                <!--/row--></section>
        </div>
    </div>
</div>