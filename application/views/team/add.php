<? if ($message) : ?>
    <h3 class="message">
        <?= $message; ?>
    </h3>
<? endif; ?>
<?php echo HTML::script("media/js/goop.js"); ?>

<?php echo HTML::style("media/css/base.css"); ?>
<?php echo HTML::style("media/css/team.css"); ?>

<div class="container">
    <div class="content ">

        <div class="main ">
            <section>
                <h1 class="header-testers subhead">

                    Create a New Team
                    <div>
                        <small>Create a team to group builds and testers.</small>
                    </div>
                </h1>
                <hr>
                <?= Form::open() ?>
                <div style="display:none">
                    <?= Form::hidden('csrf', Security::token()); ?>
                    <?= Form::hidden('date', 0); ?>
                </div>
                <div class="row">
                    <div class="span13 offset1">
                        <fieldset><!-- Team Name -->
                            <div class="clearfix "><label for="email">
                                    <strong>Team Name</strong><br>
                                    <small>A team is just a means of grouping your builds and testers.</small>
                                </label>

                                <div class="input">
                                    <!--                                        <input type="text" size="30" name="name" id="id_name"-->
                                    <!--                                                              class="span9" value="">-->
                                    <?= FORM::input('name', null, array('size' => '30', 'id' => 'id_name', 'class' => 'span9')) ?>

                                    <p class="spacetop">You may want a team per Google developer account, or a team
                                        per client you work with, or you may just have your own personal team and
                                        your day job team. You can also invite developers to help you add testers
                                        and upload builds. Get creative!</p></div>
                            </div>
                            <p class="actions right">
                                <?= Form::submit('save', 'Save »', array('class' => 'primary btn wide pull-right', 'id' => 'uploadbtn')); ?>
                            </p>
                        </fieldset>
                    </div>
                </div>
                <?= Form::close() ?>
            </section>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#id_name, #uploadbtn').click(function () {
        var dt = new Date();
        var time = dt.getFullYear() + ":" + parseInt(dt.getMonth() + 1) + ":" + dt.getDate() + ' ' + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        $("input[name='date']").val(time);
    });
    document.read(function () {
        var dt = new Date();
        var time = dt.getFullYear() + ":" + parseInt(dt.getMonth() + 1) + ":" + dt.getDate() + ' ' + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        $("input[name='date']").val(time);

    });
</script>