<? if ($message) : ?>
    <h3 class="message">
        <?= $message; ?>
    </h3>
<? endif; ?>
<?php echo HTML::script("media/js/goop.js"); ?>

<?php echo HTML::style("media/css/base.css"); ?>
<?php echo HTML::style("media/css/team.css"); ?>
<?php echo HTML::style("media/css/build_activity.css"); ?>

<div class="container">
    <div class="content ">

        <div class="main ">
            <section>
                <h1 class="header-testers subhead">

                    Accept Invitations
                    <div>
                        <small>Accept Invitations and be a tester.</small>
                    </div>
                </h1>
                <hr>
                <? foreach ($invitations as $invitation) : ?>
                    <?= Form::open() ?>
                    <div style="display:none">
                        <?= Form::hidden('csrf', Security::token()); ?>
                        <?= Form::hidden('id', $invitation->id); ?>
                    </div>
                    <div class="flexrow" id="nonteammate-div" style="">
                        <div class="column twelve">

                            <table class="zebra-striped" id="nonteammate-list">
                                <thead>
                                <tr>
                                    <th class="nopadleft"><strong>From</strong></th>
                                    <th width="110" class="center"><span
                                            class="device-sm"><strong>Invitation</strong></span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="pointer">
                                    <td>
                                        <? $team = ORM::factory('team')->where('id', '=', $invitation->team_id)->find() ?>
                                        <? $user = ORM::factory('user')->where('id', '=', $team->user_id)->find() ?>
                                        <?= $user->username ?>
                                    </td>
                                    <td>
                                        <? if ($invitation->accepted) : ?>
                                            Accepted
                                        <? else : ?>
                                            <?= Form::submit('accept', 'Accept »', array('class' => 'primary btn wide pull-right', 'id' => 'uploadbtn')); ?>
                                        <? endif; ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <?= Form::close() ?>
                <? endforeach ?>
            </section>
        </div>
    </div>
</div>

