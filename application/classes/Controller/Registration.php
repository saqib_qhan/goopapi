<?php defined('SYSPATH') or die('No direct script access.');
require_once Kohana::find_file('vendor', 'mailer/lib/swift_required');
require_once Kohana::find_file('vendor', 'mailer/lib/swift_init');

class Controller_Registration extends Controller_Template
{

    public $template = 'template';

    public function action_sign_in()
    {
        if (Auth::instance()->logged_in()) {
            HTTP::redirect('welcome/index');
        } else {
            $this->template->content = View::factory('sign_in')
                ->bind('tester', $tester)
                ->bind('team', $team)
                ->bind('team_token', $team_token)
                ->bind('message', $message);

            $tester = false;
            $team_token = false;
            if ($this->request->param('id') == "tester") {
                $tester = true;
//                if ($this->request->query('token')) {
//                    $team_token = $this->request->query('token');
//                    $team = ORM::factory('team')->where('token', '=', $team_token)->find();
//                }
            }
            if (HTTP_Request::POST == $this->request->method()) {
                // Attempt to login user
                $remember = array_key_exists('remember', $this->request->post()) ? (bool)$this->request->post('remember') : FALSE;
                $user = Auth::instance()->login($this->request->post('username'), $this->request->post('password'), $remember);

                // If successful, redirect user to index
                if ($user) {
//                    HTTP::redirect('welcome/index');
                    $get_user = Auth::instance()->get_user();
                    $team = ORM::factory('team')->where('user_id', '=', $get_user->id)->find();

//                    if ($this->request->post('team_token')) {
//                        $find_team = ORM::factory('team')->where('token', '=', $this->request->post('team_token'))->find();
//                        $teammember = ORM::factory('teammember')->where('email', '=', $this->request->post('username'))->and_where('team_id', '=', $find_team->id)->find();
//                        if ($teammember->loaded()) {
//                            $teammember->accepted = true;
//                            $teammember->update();
//                        }
//                    }

                    if ($get_user->tester) {
                        HTTP::redirect('/team/invitations');
                    } else {
                        if ($team->loaded()) {
                            HTTP::redirect('build/upload');
                        } else {
                            HTTP::redirect('team/add');
                        }
                    }

                } else {
                    $message = 'Login failed';
                }
            }
        }
    }

    public function action_sign_up()
    {
        if (Auth::instance()->logged_in()) {
            HTTP::redirect('welcome/index');
        } else {
            $this->template->content = View::factory('sign_up')
                ->bind('errors', $errors)
                ->bind('tester', $tester)
                ->bind('team', $team)
                ->bind('team_token', $team_token)
                ->bind('message', $message);
            $tester = false;
            $team_token = false;
            if ($this->request->param('id') == "tester") {
                $tester = true;
//                if ($this->request->query('token')) {
//                    $team_token = $this->request->query('token');
//                    $team = ORM::factory('team')->where('token', '=', $team_token)->find();
//                }
            }
            if (HTTP_Request::POST == $this->request->method()) {
                try {

                    // Create the user using form values
                    $user = ORM::factory('user')->create_user($this->request->post(), array(
                        'username',
                        'password',
                        'email',
                        'developer'
                    ));
                    $user->tester = $this->request->post('tester');
                    $user->update();

                    // Grant user login role
                    $user->add('roles', ORM::factory('role', array('name' => 'login')));
//                    if ($team_token) {
//                        $find_team = ORM::factory('team')->where('id', '=', $this->request->post('team'))->find();
//                        $teammember = ORM::factory('teammember')->where('email', '=', $this->request->post('email'))->and_where('team_id', '=', $find_team->id)->find();
//                        if ($teammember->loaded()) {
//                            $teammember->accepted = true;
//                            $teammember->update();
//                        }
//                    }
                    // Reset values so form is not sticky
                    $_POST = array();

                    // Set success message
                    // $message = "Account Created '{$user->username}' to the database";
                    $config = Kohana::$config->load('email')->get('options');
                    $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                        ->setUsername($config['username'])
                        ->setPassword($config['password']);

                    $mailer = Swift_Mailer::newInstance($transport);


                    // Create the message
                    $message = Swift_Message::newInstance()

                        // Give the message a subject
                        ->setSubject('Welcome to Goop. IOS Tester')

                        // Set the From address with an associative array
                        ->setFrom(array($config['username'] => 'Goop.Team'))

                        // Set the To addresses with an associative array
                        ->setTo(array($user->email => $user->username))

                        // Give it a body
                        ->setBody('Your Registration was successful. ');

                    $result = $mailer->send($message);

                    $message = "Account Created Successfully.";
                    HTTP::redirect('registration/sign_in');

                } catch (ORM_Validation_Exception $e) {

                    // Set failure message
                    $message = 'There were errors, please see form below.';

                    // Set errors using custom messages
                    $errors = $e->errors('models');
                }
            }
        }
    }

    public function action_logout()
    {
        // Log user out
        Auth::instance()->logout();

        // Redirect to login page
        HTTP::redirect('registration/sign_in');
    }

} // End
?>
