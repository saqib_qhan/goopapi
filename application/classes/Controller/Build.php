<?php defined('SYSPATH') or die('No direct script access.');
require_once Kohana::find_file('vendor', 'mailer/lib/swift_required');
require_once Kohana::find_file('vendor', 'mailer/lib/swift_init');

require_once 'vendor/autoload.php';

class Controller_Build extends Controller_Template
{
    public $template = 'template';
    private $goop_app = null;
    private $goop_version_name = null;
    private $goop_version_code = null;
    private $goop_file_name = null;
    private $goop_file_size = null;
    private $goop_errors = null;
    private $goop_date_time = null;

    public function before()
    {
        if (in_array($this->request->action(), array('edit', 'activity', 'permissions'))) {
            $this->template = 'build_template';
        }
        $this->auth = Auth::instance();
        if (!$this->auth->logged_in()) {
            if (in_array($this->request->action(), array('install'))) {
                $emailbuild = ORM::factory('emailbuild')->where('token', '=', $this->request->param('id'))->find();
                $appbuild = ORM::factory('appbuild')->where('id', '=', $emailbuild->build_id)->find();
                $app = ORM::factory('app')->where('id', '=', $appbuild->app_id)->find();
                $team = ORM::factory('team')->where('id', '=', $app->team_id)->find();

                HTTP::redirect("/registration/sign_up/tester?token={$team->token}");
            } else {
                HTTP::redirect('/registration/sign_in');
            }
        }
        parent::before();
    }

    public function action_upload()
    {
        $this->template->content = View::factory('build/add')
            ->bind('message', $message)
            ->bind('team', $team);

        $get_user = Auth::instance()->get_user();
        $team = ORM::factory('team')->where('user_id', '=', $get_user->id)->and_where('selected', '=', true)->find();
        if ($team->loaded()) {
            if (HTTP_Request::POST == $this->request->method() && $this->request->post('csrf') === Security::token() && $this->request->post('csrf') != '') {
                $this->goop_date_time = $this->request->post('date');
                if (isset($_FILES['binary'])) {
                    $filename = $this->_save_file($_FILES['binary']["name"], $_FILES['binary']["error"]
                        , $_FILES['binary']["type"], $_FILES['binary']["size"]
                        , $_FILES['binary']["tmp_name"]);

                    $this->auto_render = FALSE;
                    $this->is_ajax = TRUE;
                    $this->request->headers['Content-Type'] = 'application/json';
                    if (!$filename) {
                        $this->response->body(json_encode(array("errors" => array("binary" => $this->goop_errors), "help_url" => "")));
                    } else {
                        $create_build = ORM::factory('appbuild')->create_build($this->goop_app, $this->goop_version_name, $this->goop_version_code, $this->request->post('description'), $this->goop_file_name, $this->goop_file_size, $this->goop_date_time, Kohana::$config->load('team')->get('options')["team_id"]);
                        if ($create_build != false) {
                            $this->_send_email($create_build);
                            $this->response->body(json_encode(array("redirect" => "edit/{$create_build->id}")));
                        } else {
                            $this->goop_errors = "Error While Creating Build.";
                            $this->response->body(json_encode(array("errors" => array("binary" => $this->goop_errors), "help_url" => "")));
                        }
                    }

                }
            }
        } else {
            HTTP::redirect('/');
        }
    }

    public function action_edit()
    {
//        $this->request->param('id');
        $this->template->content = View::factory('build/information')
            ->bind('message', $message)
            ->bind('app', $app)
            ->bind('user', $user)
            ->bind('build', $build);
        $user = Auth::instance()->get_user();
        $build = ORM::factory('appbuild')->where('id', '=', $this->request->param('id'))->find();
        $this->template->build = $build;
        $app = ORM::factory('app')->where('id', '=', $build->app_id)->find();
        $this->template->app = $app;

        if ($user->id == $app->user_id) {

            if (HTTP_Request::POST == $this->request->method() && $this->request->post('csrf') === Security::token() && $this->request->post('csrf') != '') {
                if (HTTP_Request::POST == $this->request->method()) {
                    $build->updated_at = $this->request->post('date');
                    $build->release_notes = $this->request->post('description');
                    $build->update();
                }
            }

        } else {
            HTTP::redirect('/');
        }


    }

    public function action_activity()
    {
        $this->template->content = View::factory('build/activity')
            ->bind('message', $message)
            ->bind('app', $app)
            ->bind('user', $user)
            ->bind('activity', $activity)
            ->bind('emailbuilds', $emailbuilds)
            ->bind('$emailbuilds_exist', $emailbuilds_exist)
            ->bind('testers', $testers)
            ->bind('build', $build);
        $user = Auth::instance()->get_user();
        $build = ORM::factory('appbuild')->where('id', '=', $this->request->param('id'))->find();
        $this->template->build = $build;
        $app = ORM::factory('app')->where('id', '=', $build->app_id)->find();
        $this->template->app = $app;
        if ($user->id == $app->user_id) {
            $team_id = Kohana::$config->load('team')->get('options')["team_id"];
            $testers = ORM::factory('teammember')->where('team_id', '=', $team_id)->find_all();
            $emailbuilds = ORM::factory('emailbuild')->where('build_id', '=', $build->id)->find_all();
            $emailbuilds_exist = ORM::factory('emailbuild')->where('build_id', '=', $build->id)->find();
            $activity = false;
            if ($emailbuilds_exist->loaded()) {
                $activity = true;
            }
            if ($user->id != $app->user_id) {
                HTTP::redirect('/');
            }
        } else {
            HTTP::redirect('/');
        }

    }

    public function action_permissions()
    {
        $this->template->content = View::factory('build/permissions')
            ->bind('message', $message)
            ->bind('app', $app)
            ->bind('user', $user)
            ->bind('activity', $activity)
            ->bind('emailbuilds', $emailbuilds)
            ->bind('emailbuilds_exist', $emailbuilds_exist)
            ->bind('teammates_available', $teammates_available)
            ->bind('testers', $testers)
            ->bind('build', $build);
        $user = Auth::instance()->get_user();
        $team_id = Kohana::$config->load('team')->get('options')["team_id"];
        $testers = ORM::factory('teammember')->where('team_id', '=', $team_id)->find_all();
        if (HTTP_Request::POST == $this->request->method() && $this->request->post('csrfmiddlewaretoken') === Security::token() && $this->request->post('csrfmiddlewaretoken') != '') {
            $build = ORM::factory('appbuild')->where('id', '=', $this->request->post('id'))->find();
            $app = ORM::factory('app')->where('id', '=', $build->app_id)->find();
            $user_test = array();
            if ($user->id == $app->user_id) {
                foreach ($this->request->post('testers') as $test) {
                    array_push($user_test, $test);
                }
                foreach ($testers as $test) {
                    if (in_array($test->id, $user_test)) {
                        $test->permitted = true;
                    } else {
                        $test->permitted = false;
                    }
                    $test->update();
                }
                $message = "successfully updated.";
                HTTP::redirect("/build/permissions/{$build->id}");
            } else {
                HTTP::redirect('/');
            }
        } else {
            $build = ORM::factory('appbuild')->where('id', '=', $this->request->param('id'))->find();
            $app = ORM::factory('app')->where('id', '=', $build->app_id)->find();
        }
        $this->template->build = $build;
        $this->template->app = $app;
        if ($user->id == $app->user_id) {
            $testers_exist = ORM::factory('teammember')->where('team_id', '=', $team_id)->find();
            $emailbuilds = ORM::factory('emailbuild')->where('build_id', '=', $build->id)->find_all();
            $emailbuilds_exist = ORM::factory('emailbuild')->where('build_id', '=', $build->id)->find();
            $activity = false;
            $teammates_available = false;
            if ($emailbuilds_exist->loaded()) {
                $activity = true;
            }
            if ($testers_exist->loaded()) {
                $teammates_available = true;
            }
            if ($user->id != $app->user_id) {
                HTTP::redirect('/');
            }
        } else {
            HTTP::redirect('/');
        }

    }


    public function action_install()
    {
//      $this->request->param('id');
        $this->template->content = View::factory('build/install')
            ->bind('message', $message)
            ->bind('user', $user)
            ->bind('app', $app)
            ->bind('emailbuild', $emailbuild)
            ->bind('build', $build);
        if (HTTP_Request::POST == $this->request->method() && $this->request->post('csrf') === Security::token() && $this->request->post('csrf') != '') {
            $emailbuild = ORM::factory('emailbuild')->where('token', '=', $this->request->post('id'))->find();
        } else {
            $emailbuild = ORM::factory('emailbuild')->where('token', '=', $this->request->param('id'))->find();
        }

        if ($emailbuild->loaded()) {
            $build = ORM::factory('appbuild')->where('id', '=', $emailbuild->build_id)->find();
            $teammember = ORM::factory('teammember')->where('id', '=', $emailbuild->teammember_id)->find();
            if ($teammember->loaded() && $teammember->permitted) {
                if ($build->loaded()) {
                    $app = ORM::factory('app')->where('id', '=', $build->app_id)->find();
                    // find the last(updated build)
                    $appbuild = ORM::factory('appbuild')->where('app_id', '=', $app->id)->order_by('id', 'DESC')->limit(1)->find();
                    $emailbuild->email = "Opened";
                    $emailbuild->accepted = true;
                    $emailbuild->update();

                    if (HTTP_Request::POST == $this->request->method() && $this->request->post('csrf') === Security::token() && $this->request->post('csrf') != '') {
                        $file = "uploads/{$appbuild->file_name}";

                        if (file_exists($file)) {
                            header('Content-Description: File Transfer');
                            header('Content-Type: application/octet-stream');
                            header('Content-Disposition: attachment; filename=' . basename($file));
                            header('Expires: 0');
                            header('Cache-Control: must-revalidate');
                            header('Pragma: public');
                            header('Content-Length: ' . filesize($file));
                            ob_clean();
                            flush();
                            readfile($file);
                            $emailbuild->installed = true;
                            $emailbuild->update();

                            exit;
                        }
                    }
                }
            } else {
                HTTP::redirect('/');
            }
        } else {
//            HTTP::redirect('/');
        }
    }

    public function action_delete()
    {
//        $this->request->param('id');
        $this->template->content = View::factory('build/add')
            ->bind('message', $message);

        $build = ORM::factory('appbuild')->where('id', '=', $this->request->param('id'))->find();
        $this->template->build = $build;
        $app = ORM::factory('app')->where('id', '=', $build->app_id)->find();
        $this->template->app = $app;
        $user = Auth::instance()->get_user();

        if ($user->id == $app->user_id) {
            $build->delete();
            HTTP::redirect('/build/upload');
        } else {
            HTTP::redirect('/');
        }

    }

    protected function _save_file($file_name, $file_error, $file_type, $file_size, $file_tmp_name)
    {
        $allowedExts = array("apk");
        $temp = explode(".", $file_name);
        $extension = end($temp);
        if ($file_type == "application/vnd.android.package-archive" && $file_size < 8000000 && in_array($extension, $allowedExts)) {
            if ($file_error > 0) {
//                echo "Return Code: " . $file_error . "<br>";
                $this->goop_errors = "Error!! {$file_error}";
                return false;
            } else {
//                echo "Upload: " . $file_name . "<br>";
//                echo "Type: " . $file_type . "<br>";
//                echo "Size: " . ($file_size / 1024) . " kB<br>";
//                echo "Temp file: " . $file_tmp_name . "<br>";
                if (file_exists("uploads/" . $file_name)) {
//                    echo $file_name . " already exists. ";
                    $file_name = $this->_already_exist($file_name);
                }
                move_uploaded_file($file_tmp_name,
                    "uploads/" . $file_name);
//                echo "Stored in: " . "uploads/" . $file_name;
                $read_apk = $this->_read_apk($file_name);
                if (!$read_apk) {
                    return false;
                } else {
                    $member = ORM::factory('app');
                    $app_exist = $member->where('bundle', '=', $read_apk->getPackageName())->find();
                    if ($app_exist->loaded()) {
                        $this->goop_app = $app_exist;
                        $this->goop_version_name = $read_apk->getVersionName();
                        $this->goop_version_code = $read_apk->getVersionCode();
                        $this->goop_file_name = $file_name;
                        $this->goop_file_size = $file_size;
                    } else {
                        $apps = ORM::factory('app')->create_app($read_apk, $this->goop_date_time);
                        if ($apps != false) {
                            $this->goop_app = $apps;
                            $this->goop_version_name = $read_apk->getVersionName();
                            $this->goop_version_code = $read_apk->getVersionCode();
                            $this->goop_file_name = $file_name;
                            $this->goop_file_size = $file_size;
                        }
                    }
                    return $read_apk;
                }
            }
        } else {
//            echo "Invalid file";
            $this->goop_errors = "Error!!Make sure you upload a valid apk file with size < 800MB ";
            return false;
        }
    }

    protected function _read_apk($filename)
    {
        try {
            $apk = new \ApkParser\Parser("uploads/" . $filename);
            $manifest = $apk->getManifest();
            $permissions = $manifest->getPermissions();

//            echo '<pre>';
//            echo "Package Name      : " . $manifest->getPackageName()  . "\r\n";
//            echo "Version           : " . $manifest->getVersionName() . " (" . $manifest->getVersionCode() . ")\r\n";
//            echo "Min Sdk Level     : " . $manifest->getMinSdkLevel() . "\r\n";
//            echo "Min Sdk Platform  : " . $manifest->getMinSdk()  ->platform . "\r\n";
//
//            echo "------------- Permissions List -------------\r\n";

            // find max length to print more pretty.
            $perm_keys = array_keys($permissions);
            $perm_key_lengths = array_map(function ($perm) {
                return strlen($perm);
            }, $perm_keys);
            $max_length = max($perm_key_lengths);

            foreach ($permissions as $perm => $description) {
//                echo str_pad($perm, $max_length + 4, ' ') . "=> " . $description . " \r\n";
            }
            return $manifest;
        } catch (Exception $e) {
//            $this->template->errors[] = $e->getMessage();
            $this->goop_errors = "Error!! apk is not well formatted.";
            return false;
        }

    }

    protected function _already_exist($file_name)
    {
        $file_name = strtolower(Text::random('alnum', 20)) . $file_name;
        if (file_exists("uploads/" . $file_name)) {
            $this->_already_exist($file_name);
        } else {
            return $file_name;
        }
    }

    protected function _send_email($build)
    {
        $team_id = Kohana::$config->load('team')->get('options')["team_id"];
        $testers = ORM::factory('teammember')->where('team_id', '=', $team_id)->find_all();
        $user = Auth::instance()->get_user();
        $mailing_address = array();
        try {
            foreach ($testers as $test) {
                array_push($mailing_address, $test->email);
                $emailbuild = ORM::factory('emailbuild')->create_emailbuild($build, $user, "Pending", $test);
                $config = Kohana::$config->load('email')->get('options');
                $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                    ->setUsername($config['username'])
                    ->setPassword($config['password']);

                $mailer = Swift_Mailer::newInstance($transport);

                // Create the message
                $message = Swift_Message::newInstance()

                    // Give the message a subject
                    ->setSubject('Build Uploaded')

                    // Set the From address with an associative array
                    ->setFrom(array($config['username'] => 'Goop.Team'))

                    // Set the To addresses with an associative array
                    ->setTo($test->email)

                    // Give it a body
                    ->setBody("Build Uploaded successfully. Please open the link through your android device to install. <br/> http://localhost/goopiosapptest/build/install/{$emailbuild->token}");

                $result = $mailer->send($message);
                if ($result) {
                    $emailbuild->email = "Sent";
                    $emailbuild->update();
                } else {
                    $emailbuild->email = "Failed";
                }

            }
            return true;

        } catch (Exception $e) {
            return false;
        }
    }

}
