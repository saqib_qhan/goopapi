<?php defined('SYSPATH') or die('No direct script access.');

require_once 'vendor/autoload.php';


class Controller_Api_Goopapiv1 extends Controller
{
    private $status_ok = 200;
    private $status_failed = 500;

    public function action_sessions()
    {
        try {
            $app = ORM::factory('app')->where('app_token', '=', $this->request->post('app_token'))->find();

            if ($app->loaded()) {
                $appbuild = ORM::factory('appbuild')->where('app_id', '=', $app->id)->order_by('id', 'DESC')->limit(1)->find();
                $session = ORM::factory('session')->where('app_token', '=', $this->request->post('app_token'))->and_where('device_id', '=', $this->request->post('device_id'))->and_where('session_ended', '=', false)->find();
                if ($this->request->post('session_type') == 'start_session') {
                    if (!$session->loaded()) {
                        $create_session = ORM::factory('session')->start_session($this->request, $appbuild);
                        if ($create_session) {
                            $this->response->body(json_encode(array("status" => $this->status_ok, array("data" => array("message" => "session started successfully.")))));
                        } else {
                            $this->response->body(json_encode(array("status" => $this->status_failed, "data" => array("message" => "something went wrong."))));
                        }
                    } else {
                        $this->response->body(json_encode(array("status" => $this->status_failed, "data" => array("message" => "session already open."))));
                    }

                } else if ($this->request->post('session_type') == 'end_session') {
                    if ($session->loaded()) {
                        $terminate_session = ORM::factory('session')->where('app_token', '=', $this->request->post('app_token'))->and_where('device_id', '=', $this->request->post('device_id'))->and_where('session_ended', '=', false)->find();
                        if ($terminate_session->loaded()) {
                            $update_session = ORM::factory('session')->end_session($this->request, $terminate_session);
                            if ($update_session) {
                                $this->response->body(json_encode(array("status" => $this->status_ok, "data" => array("message" => "session ended successfully."))));
                            } else {
                                $this->response->body(json_encode(array("status" => $this->status_failed, "data" => array("message" => "something went wrong."))));
                            }
                        } else {
                            $this->response->body(json_encode(array("status" => $this->status_failed, "data" => array("message" => "no such session found."))));
                        }
                    } else {
                        $this->response->body(json_encode(array("status" => $this->status_failed, "data" => array("message" => "no such session found."))));
                    }
                }
            } else {
                $this->response->body(json_encode(array("status" => $this->status_failed, "data" => array("message" => "Incorrect App Token."))));
            }
        } catch (Exception $e) {
            $this->response->body(json_encode(array("status" => $this->status_failed, "data" => array("message" => "something went wrong."))));
        }

    }

    public function action_sign_in()
    {
        try {
            $_user = ORM::factory('user')->where('email', '=', $this->request->query('email'))->find();
            $user = Auth::instance()->login($this->request->query('email'), $this->request->query('password'), false);
            if ($user) {
                $device = ORM::factory('userdevice')->where('device_id', '=', $this->request->query('device_id'))->and_where('user_id', '=', $_user->id)->find();
                if (!$device->loaded()) {
                    $create_userdevice = ORM::factory("userdevice")->create_userdevice($this->request, $_user);
                    if ($create_userdevice) {
                        $this->response->body(json_encode(array("status" => $this->status_ok, "data" => array("message" => "successfully Logged In."))));
                    } else {
                        $this->response->body(json_encode(array("status" => $this->status_failed, "data" => array("message" => "something went wrong."))));
                    }
                }
                else{
                    $this->response->body(json_encode(array("status" => $this->status_ok, "data" => array("message" => "successfully Logged In."))));
                }
            } else {
                $this->response->body(json_encode(array("status" => $this->status_failed, "data" => array("message" => "Invalid username or password."))));
            }
        } catch (Exception $e) {
            $this->response->body(json_encode(array("status" => $this->status_failed, "data" => array("message" => "something went wrong."))));
        }
    }

}
