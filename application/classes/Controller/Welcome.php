<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller
{

    public function action_index()
    {
        $user = Auth::instance()->get_user();
        if ($user) {
            if ($user->tester) {
                HTTP::redirect("/team/invitations");
            }
        }
        $this->response->body('welcome, goop tester!');
    }

} // End Welcome
