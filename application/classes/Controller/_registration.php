<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Registration extends Controller {

	public function action_sign_in()
	{
		$this->template->content = View::factory('sign_in')
			->bind('user', $user);
		
		// Load the user information
		$user = Auth::instance()->get_user();
		
		// if a user is not logged in, redirect to login page
		// if (!$user)
 		//{
	//		Request::current()->redirect('user/login');
	//	}
	}

	public function action_submit_sign_in() 
	{
		$this->template->content = View::factory('sign_in')
			->bind('message', $message);
			
		if (HTTP_Request::POST == $this->request->method()) 
		{
			// Attempt to login user
		$remember = array_key_exists('remember', $this->request->post()) ? (bool) $this->request->post('remember') : FALSE;
			$user = Auth::instance()->login($this->request->post('username'), $this->request->post('password'), $remember);
			
			// If successful, redirect user
			if ($user) 
			{
				Request::current()->redirect('welcome/index');
			} 
			else 
			{
				$message = 'Login failed';
			}
		}
	}

} // End
?>
