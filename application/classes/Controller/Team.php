<?php defined('SYSPATH') or die('No direct script access.');
require_once Kohana::find_file('vendor', 'mailer/lib/swift_required');
require_once Kohana::find_file('vendor', 'mailer/lib/swift_init');

require_once 'vendor/autoload.php';


class Controller_Team extends Controller_Template
{

    public function before()
    {
        $this->auth = Auth::instance();
        if (!$this->auth->logged_in()) {
            HTTP::redirect('/registration/sign_in');
        }
        parent::before();
    }

    public function action_add()
    {
        try {
            $this->template->content = View::factory('team/add')
                ->bind('message', $message);

            if (HTTP_Request::POST == $this->request->method() && $this->request->post('csrf') === Security::token() && $this->request->post('csrf') != '') {
                $get_user = Auth::instance()->get_user();
                $already_teams = ORM::factory('team')->where('user_id', '=', $get_user->id)->find_all();
                foreach ($already_teams as $val) {
                    $val->selected = false;
                    $val->update();
                }

                $create_team = ORM::factory('team')->create_team($this->request, $get_user);
                HTTP::redirect('/team/member');
            }
        } catch (ORM_Validation_Exception $e) {
            // Set failure message
            $message = $e->errors('team')['name'];
        }
    }

    public function action_member()
    {
        try {
            $this->template->content = View::factory('team/member')
                ->bind('message', $message);
            $get_user = Auth::instance()->get_user();

            if (HTTP_Request::POST == $this->request->method() && $this->request->post('csrf') === Security::token() && $this->request->post('csrf') != '') {
                $sendTo = explode(',', $this->request->post('email'));
                foreach ($sendTo as $email) {
                    $create_teammember = ORM::factory('teammember')->create_teammember($this->request, $get_user, $email);
                }
                $send_email = $this->_send_email($this->request, $get_user, $sendTo);
                if ($send_email) {
                    HTTP::redirect('build/upload');
                }
            }
        } catch (ORM_Validation_Exception $e) {
            $message = $e->errors('teammember')['email'];
        }
    }

    public function action_invitations()
    {
        try {
            $this->template->content = View::factory('team/invitations')
                ->bind('invitations', $invitations)
                ->bind('message', $message);
            $get_user = Auth::instance()->get_user();
            $invitations = ORM::factory('teammember')->where('email', '=', $get_user->email)->find_all();
            if (HTTP_Request::POST == $this->request->method() && $this->request->post('csrf') === Security::token() && $this->request->post('csrf') != '') {
                $teammeber = ORM::factory('teammember')->where('id', '=', $this->request->post('id'))->find();
                if ($teammeber->loaded()) {
                    $teammeber->accepted = true;
                    $teammeber->update();
                }
                HTTP::redirect("/team/invitations");
            }
        } catch (ORM_Validation_Exception $e) {
            $message = "Something went wrong.";
        }
    }

    protected function _send_email($request, $user, $sendTo)
    {
        try {
            $team_id = Kohana::$config->load('team')->get('options')["team_id"];
            $team = ORM::factory('team')->where('id', '=', $team_id)->find();
            $config = Kohana::$config->load('email')->get('options');
            $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername($config['username'])
                ->setPassword($config['password']);

            $mailer = Swift_Mailer::newInstance($transport);


            // Create the message
            $message = Swift_Message::newInstance()

                // Give the message a subject
                ->setSubject('Tester Invitation')

                // Set the From address with an associative array
                ->setFrom(array($config['username'] => 'Goop.Team'))

                // Set the To addresses with an associative array
                ->setTo($sendTo)

                // Give it a body
                ->setBody($request->post('message') . "{$user->username} has invited you to be a tester. Click the link to accept. http://{$_SERVER['SERVER_NAME']}/goopiosapptest/registration/sign_in/tester");

            $result = $mailer->send($message);
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

}// End Team
