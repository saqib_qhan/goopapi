<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Userdevice extends ORM
{

    protected $_belongs_to = array(
        'user' => array('model' => 'User')
    );

    public function rules()
    {
        return array(
            'device_id' => array(
                array('not_empty')
            )
        );
    }

    public function create_userdevice($request, $user)
    {
        #http://localhost/goopiosapptest/api/goopapiv1/sign_in?device_id=abc&email=abc@abc.com&password=abc
        $this->device_id = $request->query('device_id');
        $this->user_id = $user->id;
        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

} // End Userdevice Model
