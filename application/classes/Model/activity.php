<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Activity extends ORM
{

    protected $_belongs_to = array(
        'emailbuild' => array('model' => 'Emailbuild'),
        'platform' => array('model' => 'Platform')
    );

 } // End Activity Model
