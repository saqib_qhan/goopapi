<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Teammember extends ORM
{

    protected $_belongs_to = array(
        'team' => array('model' => 'Team'),
        'emailbuilds' => array('model' => 'Emailbuild')

    );

    public function rules()
    {
        return array(
            'email' => array(
                array('not_empty')
            )
        );
    }

    public function create_teammember($request, $user, $email)
    {
        $this->email = $email;
        $this->team_id = Kohana::$config->load('team')->get('options')["team_id"];
        $this->developer = $request->post('developer');
        $this->save();
    }

} // End Teammember Model
