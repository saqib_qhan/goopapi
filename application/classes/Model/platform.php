<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Platform extends ORM
{
    protected $_has_many = array(
        'apps' => array('model' => 'App'),
        'activities' => array('model' => 'Activity')
    );
} // End Platform Model
