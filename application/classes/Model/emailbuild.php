<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Emailbuild extends ORM
{

    protected $_belongs_to = array(
        'appbuild' => array('model' => 'Appbuild'),
        'user' => array('model' => 'User'),
        'teammember' => array('model' => 'Teammember'),
    );

    protected $_has_many = array(
        'activties' => array('model' => 'Activity')
    );

    public function create_emailbuild($build, $user, $status, $teammember)
    {
        try {
            $this->email = $status;
            $this->user_email = $teammember->email;
            $this->installed = false;
            $this->build_id = $build->id;
            $this->teammember_id = $teammember->id;
            $token = md5(uniqid(rand(), true));
            $find_token = ORM::factory('emailbuild')->where('token', '=', $token)->find();
            if ($find_token->loaded()) {
                $find_token = $this->generate_token();
            } else {
                $find_token = $token;
            }
            $this->token = $find_token;
            $emailbuild = $this->save();

            return $emailbuild;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    protected function generate_token()
    {
        $token = md5(uniqid(rand(), true));
        $find_token = ORM::factory('emailbuild')->where('token', '=', $token)->find();
        if ($find_token->loaded()) {
            $find_token = $this->generate_token();
        } else {
            $find_token = $token;
        }
        return $find_token;
    }
} // End Emailbuild Model
