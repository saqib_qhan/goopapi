<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Appbuild extends ORM
{

    protected $_belongs_to = array(
        'app' => array('model' => 'App')
    );

    protected $_has_many = array(
        'emailbuilds' => array('model' => 'Emailbuild'),
        'sessions' => array('model' => 'Session')

    );

    public function create_build($app, $versionName, $versionCode, $description, $file_name, $file_size, $datetime, $team_id)
    {
//        echo $manifest->getApplicationName();
        try {
            $this->app_id = $app->id;
            $this->file_name = $file_name;
            $this->file_size = $file_size;
            $this->version_name = $versionName;
            $this->version_code = $versionCode;
            $this->built_for = "Android";
            $this->created_at = $datetime;
//            $this->team_id = $team_id;
            $this->updated_at = $datetime;
            $this->release_notes = $description;
            $build = $this->save();
            return $build;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
} // End App Model
