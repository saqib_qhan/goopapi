<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Team extends ORM
{

    protected $_has_many = array(
        'teammembers' => array('model' => 'Teammember'),
        'apps' => array('model' => 'App')
    );
    protected $_belongs_to = array(
        'user' => array('model' => 'User')
    );


    public function rules()
    {
        return array(
            'name' => array(
                array('not_empty'),
                array('min_length', array(':value', 3)),
                array('max_length', array(':value', 32)),
                array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
            ),
            'created_at' => array(
                array('not_empty')
            )
        );
    }


    public function create_team($request, $user)
    {
        $this->name = $request->post('name');
        $this->created_at = $request->post('date');
        $this->user_id = $user->id;
        $this->selected = true;
        $token = md5(uniqid(rand(), true));
        $find_token = ORM::factory('team')->where('token', '=', $token)->find();
        if ($find_token->loaded()) {
            $find_token = $this->generate_token();
        } else {
            $find_token = $token;
        }
        $this->token = $find_token;

        $this->save();
    }

    protected function generate_token()
    {
        $token = md5(uniqid(rand(), true));
        $find_token = ORM::factory('team')->where('token', '=', $token)->find();
        if ($find_token->loaded()) {
            $find_token = $this->generate_token();
        } else {
            $find_token = $token;
        }
        return $find_token;
    }
} // End Team Model
