<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Session extends ORM
{

    protected $_belongs_to = array(
        'appbuild' => array('model' => 'Appbuild')
    );

    public function rules()
    {
        return array(
            'device_id' => array(
                array('not_empty')
            ),
            'app_token' => array(
                array('not_empty')
            )
        );
    }

    public function start_session($request, $appbuild)
    {
        #http://localhost/goopiosapptest/api/goopapiv1/sessions?app_token=8205c382c50d6bc934d92a6b43eb779d&session_type=start_session&started_at=2014-02-02%2004:10:07&device_id=c4s7ui
        try {
            $this->device_id = $request->query('device_id');
            $this->app_token = $request->query('app_token');
            $this->started_at = $request->query('started_at');
            $this->session_ended = false;
            $this->appbuild_id = $appbuild->id;
            if ($this->save()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }

    }

    public function end_session($request, $session)
    {
        #http://localhost/goopiosapptest/api/goopapiv1/sessions?app_token=8205c382c50d6bc934d92a6b43eb779d&session_type=end_session&ended_at=2014-01-02%2004:10:07&device_id=c4s7ui&crash_count=2
        try {

            $session->ended_at = $request->query('ended_at');
            $seconds = strtotime($request->query('ended_at')) - strtotime($session->started_at);


            $session->session_ended = true;
            $session->crash_count = $request->query('crash_count');
            $session->sdk = $request->query('sdk');
            $session->session_time = floor($seconds/60);

            if ($session->update()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo $e;
            return false;
        }
    }


} // End Session Model
