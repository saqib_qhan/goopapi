<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_App extends ORM
{

    protected $_belongs_to = array(
        'user' => array('model' => 'User'),
        'platform' => array('model' => 'Platform'),
        'team' => array('model' => 'Team')
    );
    protected $_has_many = array(
        'appbuilds' => array('model' => 'Appbuild')
    );


    public function rules()
    {
        return array(
            'name' => array(
                array('not_empty'),
                array('min_length', array(':value', 3)),
                array('max_length', array(':value', 32)),
                array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
            ),
            'bundle' => array(
                array('not_empty'),
                array('min_length', array(':value', 1)),
                array('max_length', array(':value', 32)),
            ),
            'platform_id' => array(
                array('not_empty')
            ),
            'user_id' => array(
                array('not_empty')
            )
        );
    }

    public function create_app($manifest, $datetime)
    {
//        echo $manifest->getApplicationName();
        try {
            $team_id = Kohana::$config->load('team')->get('options')["team_id"];
            $user = Auth::instance()->get_user();
            $platform = ORM::factory('platform');
            $platform_exist = $platform->where('name', '=', 'Android')->find();
            $this->name = $manifest->getPackageName();
            $this->bundle = $manifest->getPackageName();
            $this->platform_id = $platform_exist->id;
            $this->user_id = $user->id;
            $this->team_id = $team_id;
            $this->created_at = $datetime;
            $this->updated_at = $datetime;
            $token = md5(uniqid(rand(), true));
            $find_token = ORM::factory('app')->where('app_token', '=', $token)->find();
            if ($find_token->loaded()) {
                $find_token = $this->generate_token();
            } else {
                $find_token = $token;
            }
            $this->app_token = $find_token;
            $app_created = $this->save();
            return $app_created;
        } catch (ORM_Validation_Exception $e) {
            return false;
        }
    }

    protected function generate_token()
    {
        $token = md5(uniqid(rand(), true));
        $find_token = ORM::factory('app')->where('app_token', '=', $token)->find();
        if ($find_token->loaded()) {
            $find_token = $this->generate_token();
        } else {
            $find_token = $token;
        }
        return $find_token;
    }
} // End App Model
