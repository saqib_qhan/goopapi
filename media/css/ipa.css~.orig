.content .contain {
  max-width: 900px;
}

.header-upload {
  background-image: url('../img/ico_header_upload.png');
  background-repeat: no-repeat;
  background-position: 5px top;
  padding: 5px 0 0 65px;
}

.subhead {
  line-height: 18px;
  margin-bottom: 20px;
}

.flexrow { 
  width: 100%;
}

.flexrow .offset-by-one:first-child {
  margin-left: 8.7%;
}

.flexrow .eleven.column {
  width: 91.56%;
}

.flexrow .offset-by-one {
  margin-left: 13.1%;
}

.column:first-child { 
  margin-left: 0;
}

.column {
  margin-left: 1.2%;
  float: left;
  min-height: 1px;
  position: relative;
}

.modal.fade.in {
  top: 50%;
}

.fade.in {
  opacity: 1;
}

.modal.fade {
  -webkit-transition: opacity .3s linear,top .3s ease-out;
  -moz-transition: opacity .3s linear,top .3s ease-out;
  -ms-transition: opacity .3s linear,top .3s ease-out;
  -o-transition: opacity .3s linear,top .3s ease-out;
  transition: opacity .3s linear,top .3s ease-out;
  top: -25%;
}

.fade {
  -webkit-transition: opacity .15s linear;
  -moz-transition: opacity .15s linear;
  -ms-transition: opacity .15s linear;
  -o-transition: opacity .15s linear;
  transition: opacity .15s linear;
  opacity: 0;
}

.modal {
  position: fixed;
  top: 50%;
  left: 50%;
  z-index: 11000;
  width: 700px;
  margin: -250px 0 0 -350px;
  background-color: #fff;
  border: 1px solid rgba(0,0,0,0.3);
  -webkit-border-radius: 9px;
  -moz-border-radius: 9px;
  border-radius: 9px;
  -webkit-box-shadow: 0 0 7px rgba(0,0,0,0.5);
  -moz-box-shadow: 0 0 7px rgba(0,0,0,0.5);
  box-shadow: 0 0 7px rgba(0,0,0,0.5);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding-box;
  background-clip: padding-box;
}

.hide {
  display: none;
}

.modal-header {
  -webkit-border-radius: 8px 8px 0 0;
  -moz-border-radius: 8px 8px 0 0;
  border-radius: 8px 8px 0 0; 
  padding: 3px 15px;
  background: #2b404e;
  margin-top: -3px;
}

.modal-header h3 {
  color: #fff;
  font-size: 17px;
  text-shadow: 0 -1px 0 rgba(0,0,0,0.5);
}

.modal-body {
  padding: 15px;
}

.alert-message.block-message {
  background-image: none;
  background-color: #fdf5d9;
  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
  padding: 14px;
  border-color: #fceec1;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;
  text-shadow: none;
}

.alert-message {
  position: relative;
  padding: 7px 15px;
  margin-bottom: 18px;
  color: #616161;
  background-color: #f5e8b8;
  background-repeat: repeat-x;
  background-image: -khtml-gradient(linear,left top,left bottom,from(#f9f1d4),to(#f5e8b8));
  background-image: -moz-linear-gradient(top,#f9f1d4,#f5e8b8);
  background-image: -ms-linear-gradient(top,#f9f1d4,#f5e8b8);
  background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#f9f1d4),color-stop(100%,#f5e8b8));
  background-image: -webkit-linear-gradient(top,#f9f1d4,#f5e8b8);
  background-image: -o-linear-gradient(top,#f9f1d4,#f5e8b8);
  background-image: linear-gradient(top,#f9f1d4,#f5e8b8);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f9f1d4',endColorstr='#f5e8b8',GradientType=0);
  text-shadow: -1px -1px 0 rgba(0,0,0,0.5);
  border-color: #f5e8b8 #f5e8b8 #ecd275;
  border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
  text-shadow: 0 1px 0 rgba(255,255,255,0.5);
  border-width: 1px;
  border-style: solid;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
  -moz-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
  box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
}

.status-warning {
  background: url('../img/ico_status_warning.png') no-repeat left center;
  padding: 10px 0 9px 23px;
}

h1 small { 
  font-size: 14px;
}

h1 small, h2 small, h3 small, h4 small, h5 small, h6 small {
  font-family: 'PTSansNarrow';
  font-weight: normal;
}

form .clearfix {
  margin-bottom: 18px;
}

label {
  padding-top: 6px;
  font-family: "PTSansNarrowBold","Lucida Grande","Verdana",serif;
  font-size: 20px;
  font-weight: normal;
  line-height: 20px;
  line-height: 18px;
  float: left;
  width: 240px;
  text-align: left;
  color: #616161;
}

form .input {
  margin-left: 260px;
}

#build-form .filedrop-dropZone {
  width: auto;
  border: dashed 3px #CBCBCB;
  padding: 20px;
}

.dropfile.small, .filedrop-dropZone.small {
  font-weight: normal;
  padding: 10px; 
  border-width: 2px;
}

.dropfile, .filedrop-dropZone {
  border: dashed 3px #CBCBCB;
  padding: 20px;
  color: #8E8E8E;
  font-size: 16px;
  text-align: center;
  font-weight: bold;
  overflow: hidden;
  position: relative;
  width: 360px;
}

#build-form .filedrop-message {
  background-image: url('../img/ico_drop.png');
  background-repeat: no-repeat;
  padding-left: 35px;
}

.dropfile input[type=file], .filedrop-dropZone input[type=file] {
  background-color: #fff;
  padding: initial;
  border: none;
  line-height: initial;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;
  height: 100%;
  left: 0;
  margin: 0;
  opacity: 0;
  position: absolute;
  text-align: center;
  top: 0;
  width: 100%;
  z-index: 10;
  cursor: pointer;
}

.filedrop-dropZone input {
  height: 100%;
  width: 100%;
  font-size: 100px;
  overflow: none;
  text-align: center;
  opacity: 0;
  margin: 0;
  border: solid transparent;
  background-color: red;
  position: absolute;
  z-index: 1000;
  top: 0;
  left: 0;
}

input, textarea, .uneditable-input {
  display: inline-block;
  width: 210px;
  height: 23px;
  padding: 8px;
  font-size: 20px;
  line-height: 18px;
  color: #666;
  border: 1px solid #C3C3C3;
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
  background: #FBFBFB;
}

label small {
  font-size: 14px;
  color: #999;
  font-family: "PTSansNarrow","Lucida Grande","Verdana",serif; 
  font-size: 14px;
  font-weight: normal;
  line-height: 16px;
}

input.flex97, textarea.flex97, select.flex97 {
  width: 100%;
}

input:focus, textarea:focus {
  outline: 0;
  border-color: rgba(82,168,236,0.8);
  -webkit-box-shadow: inset 0 1px 3px rgba(0,0,0,0.1),0 0 8px rgba(82,168,236,0.6);
  -moz-box-shadow: inset 0 1px 3px rgba(0,0,0,0.1),0 0 8px rgba(82,168,236,0.6);
  box-shadow: inset 0 1px 3px rgba(0,0,0,0.1),0 0 8px rgba(82,168,236,0.6);
}

textarea {
  overflow-y: auto;
  font-size: 16px;
  height: auto;
}

.btn.wide {
  padding: 7px 40px 7px 40px;
}

.btn:active {
  -webkit-box-shadow: inset 0 2px 4px rgba(0,0,0,0.25),0 1px 2px rgba(0,0,0,0.05);
  -moz-box-shadow: inset 0 2px 4px rgba(0,0,0,0.25),0 1px 2px rgba(0,0,0,0.05);
  box-shadow: inset 0 2px 4px rgba(0,0,0,0.25),0 1px 2px rgba(0,0,0,0.05);
}

.btn.primary {
  color: #fff;
  background-color: #2f7422;
  background-repeat: repeat-x;
  background-image: -khtml-gradient(linear,left top,left bottom,from(#57b43e),to(#2f7422));
  background-image: -moz-linear-gradient(top,#57b43e,#2f7422);
  background-image: -ms-linear-gradient(top,#57b43e,#2f7422);
  background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#57b43e),color-stop(100%,#2f7422));
  background-image: -webkit-linear-gradient(top,#57b43e,#2f7422);
  background-image: -o-linear-gradient(top,#57b43e,#2f7422);
  background-image: linear-gradient(top,#57b43e,#2f7422);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#57b43e',endColorstr='#2f7422',GradientType=0);
  text-shadow: -1px -1px 0 rgba(0,0,0,0.5);
  border-color: #2f7422 #2f7422 #173911;
  border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
}

.btn:hover {
  color: #333;
  background-position: 0 -10px;
  text-decoration: none;
}

.btn:hover {
color: #333;
background-position: 0 -10px;
text-decoration: none;
}
input[type=button], input[type=reset], input[type=submit] {
  width: auto;
  height: auto;
}
button, input[type="button"], input[type="reset"], input[type="submit"] {
   cursor: pointer;
  -webkit-appearance: button;
}
.btn {
  cursor: pointer;
  display: inline-block;
  background-color: #cecece;
  background-repeat: repeat-x;
  background-image: -khtml-gradient(linear,left top,left bottom,from(#fff),to(#cecece));
  background-image: -moz-linear-gradient(top,#fff,#cecece);
  background-image: -ms-linear-gradient(top,#fff,#cecece);
  background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#fff),color-stop(100%,#cecece));
  background-image: -webkit-linear-gradient(top,#fff,#cecece);
  background-image: -o-linear-gradient(top,#fff,#cecece); 
  background-image: linear-gradient(top,#fff,#cecece);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff',endColorstr='#cecece',GradientType=0);
  font-size: 14px;
  padding: 5px 10px;
  color: #555;
  line-height: normal;
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
  -webkit-box-shadow: 1px 1px 0 rgba(255,255,255,0.3) inset,-1px 1px 0 rgba(255,255,255,0.3) inset,0 1px 2px rgba(0,0,0,0.3);
  -moz-box-shadow: 1px 1px 0 rgba(255,255,255,0.3) inset,-1px 1px 0 rgba(255,255,255,0.3) inset,0 1px 2px rgba(0,0,0,0.3);
  box-shadow: 1px 1px 0 rgba(255,255,255,0.3) inset,-1px 1px 0 rgba(255,255,255,0.3) inset,0 1px 2px rgba(0,0,0,0.3);
  border: solid 1px #bbb;
  -webkit-transition: .1s linear all;
  -moz-transition: .1s linear all;
  -ms-transition: .1s linear all;
  -o-transition: .1s linear all;
  transition: .1s linear all;
}
