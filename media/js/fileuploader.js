(function () {
    function errorListFor(id) {
        var element = $("#" + id), matches = element.prev("ul.errorlist");
        if (matches.length)return $(matches[0]);
        var errorlist = $('<ul class="errorlist error-message"></ul>');
        return element.after(errorlist), $(".filedrop-dropZone").addClass("error"), errorlist
    }

    function displayError(id, message, url) {
        var element = $("#" + id);
        url ? element.after('<ul class="errorlist error-message"><li>' + message + ' <a style="text-decoration:underline" href="' + url + '">Learn More</a>.</li></ul>') : element.after('<ul class="errorlist error-message"><li>' + message + "</li></ul>"), $(".filedrop-dropZone").addClass("error")
    }

    (function (b, j) {
        if (b.cleanData) {
            var k = b.cleanData;
            b.cleanData = function (a) {
                for (var c = 0, d; (d = a[c]) != null; c++)try {
                    b(d).triggerHandler("remove")
                } catch (e) {
                }
                k(a)
            }
        } else {
            var l = b.fn.remove;
            b.fn.remove = function (a, c) {
                return this.each(function () {
                    return c || (!a || b.filter(a, [this]).length) && b("*", this).add([this]).each(function () {
                        try {
                            b(this).triggerHandler("remove")
                        } catch (d) {
                        }
                    }), l.call(b(this), a, c)
                })
            }
        }
        b.widget = function (a, c, d) {
            var e = a.split(".")[0], f;
            a = a.split(".")[1], f = e + "-" + a, d || (d = c, c = b.Widget), b.expr[":"][f] = function (h) {
                return!!b.data(h, a)
            }, b[e] = b[e] || {}, b[e][a] = function (h, g) {
                arguments.length && this._createWidget(h, g)
            }, c = new c, c.options = b.extend(!0, {}, c.options), b[e][a].prototype = b.extend(!0, c, {namespace: e, widgetName: a, widgetEventPrefix: b[e][a].prototype.widgetEventPrefix || a, widgetBaseClass: f}, d), b.widget.bridge(a, b[e][a])
        }, b.widget.bridge = function (a, c) {
            b.fn[a] = function (d) {
                var e = typeof d == "string", f = Array.prototype.slice.call(arguments, 1), h = this;
                return d = !e && f.length ? b.extend.apply(null, [!0, d].concat(f)) : d, e && d.charAt(0) === "_" ? h : (e ? this.each(function () {
                    var g = b.data(this, a), i = g && b.isFunction(g[d]) ? g[d].apply(g, f) : g;
                    if (i !== g && i !== j)return h = i, !1
                }) : this.each(function () {
                    var g = b.data(this, a);
                    g ? g.option(d || {})._init() : b.data(this, a, new c(d, this))
                }), h)
            }
        }, b.Widget = function (a, c) {
            arguments.length && this._createWidget(a, c)
        }, b.Widget.prototype = {widgetName: "widget", widgetEventPrefix: "", options: {disabled: !1}, _createWidget: function (a, c) {
            b.data(c, this.widgetName, this), this.element = b(c), this.options = b.extend(!0, {}, this.options, this._getCreateOptions(), a);
            var d = this;
            this.element.bind("remove." + this.widgetName, function () {
                d.destroy()
            }), this._create(), this._trigger("create"), this._init()
        }, _getCreateOptions: function () {
            return b.metadata && b.metadata.get(this.element[0])[this.widgetName]
        }, _create: function () {
        }, _init: function () {
        }, destroy: function () {
            this.element.unbind("." + this.widgetName).removeData(this.widgetName), this.widget().unbind("." + this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass + "-disabled ui-state-disabled")
        }, widget: function () {
            return this.element
        }, option: function (a, c) {
            var d = a;
            if (arguments.length === 0)return b.extend({}, this.options);
            if (typeof a == "string") {
                if (c === j)return this.options[a];
                d = {}, d[a] = c
            }
            return this._setOptions(d), this
        }, _setOptions: function (a) {
            var c = this;
            return b.each(a, function (d, e) {
                c._setOption(d, e)
            }), this
        }, _setOption: function (a, c) {
            return this.options[a] = c, a === "disabled" && this.widget()[c ? "addClass" : "removeClass"](this.widgetBaseClass + "-disabled ui-state-disabled").attr("aria-disabled", c), this
        }, enable: function () {
            return this._setOption("disabled", !1)
        }, disable: function () {
            return this._setOption("disabled", !0)
        }, _trigger: function (a, c, d) {
            var e = this.options[a];
            c = b.Event(c), c.type = (a === this.widgetEventPrefix ? a : this.widgetEventPrefix + a).toLowerCase(), d = d || {};
            if (c.originalEvent) {
                a = b.event.props.length;
                for (var f; a;)f = b.event.props[--a], c[f] = c.originalEvent[f]
            }
            return this.element.trigger(c, d), !(b.isFunction(e) && e.call(this.element[0], c, d) === !1 || c.isDefaultPrevented())
        }}
    })(jQuery), function ($) {
        "use strict";
        var counter = 0;
        $.ajaxTransport("iframe", function (options, originalOptions, jqXHR) {
            if (options.type === "POST" || options.type === "GET") {
                var form, iframe;
                return{send: function (headers, completeCallback) {
                    form = $('<form style="display:none;"></form>'), iframe = $('<iframe src="javascript:false;" name="iframe-transport-' + (counter += 1) + '"></iframe>').bind("load", function () {
                        var fileInputClones;
                        iframe.unbind("load").bind("load", function () {
                            var response;
                            try {
                                response = iframe.contents();
                                if (!response.length || !response[0].firstChild)throw new Error
                            } catch (e) {
                                response = undefined
                            }
                            completeCallback(200, "success", {iframe: response}), $('<iframe src="javascript:false;"></iframe>').appendTo(form), form.remove()
                        }), form.prop("target", iframe.prop("name")).prop("action", options.url).prop("method", options.type), options.formData && $.each(options.formData, function (index, field) {
                            $('<input type="hidden"/>').prop("name", field.name).val(field.value).appendTo(form)
                        }), options.fileInput && options.fileInput.length && options.type === "POST" && (fileInputClones = options.fileInput.clone(), options.fileInput.after(function (index) {
                            return fileInputClones[index]
                        }), options.paramName && options.fileInput.each(function () {
                            $(this).prop("name", options.paramName)
                        }), form.append(options.fileInput).prop("enctype", "multipart/form-data").prop("encoding", "multipart/form-data")), form.submit(), fileInputClones && fileInputClones.length && options.fileInput.each(function (index, input) {
                            var clone = $(fileInputClones[index]);
                            $(input).prop("name", clone.prop("name")), clone.replaceWith(input)
                        })
                    }), form.append(iframe).appendTo("body")
                }, abort: function () {
                    iframe && iframe.unbind("load").prop("src", "javascript".concat(":false;")), form && form.remove()
                }}
            }
        }), $.ajaxSetup({converters: {"iframe text": function (iframe) {
            return iframe.text()
        }, "iframe json": function (iframe) {
            return $.parseJSON(iframe.text())
        }, "iframe html": function (iframe) {
            return iframe.find("body").html()
        }, "iframe script": function (iframe) {
            return $.globalEval(iframe.text())
        }}})
    }(jQuery), function ($) {
        "use strict";
        $.widget("blueimp.fileupload", {options: {namespace: undefined, dropZone: $(document), fileInput: undefined, replaceFileInput: !0, paramName: undefined, singleFileUploads: !0, limitMultiFileUploads: undefined, sequentialUploads: !1, limitConcurrentUploads: undefined, forceIframeTransport: !1, multipart: !0, maxChunkSize: undefined, uploadedBytes: undefined, recalculateProgress: !0, formData: function (form) {
            return form.serializeArray()
        }, add: function (e, data) {
            data.submit()
        }, processData: !1, contentType: !1, cache: !1}, _refreshOptionsList: ["namespace", "dropZone", "fileInput"], _isXHRUpload: function (options) {
            var undef = "undefined";
            return!options.forceIframeTransport && typeof XMLHttpRequestUpload !== undef && typeof File !== undef && (!options.multipart || typeof FormData !== undef)
        }, _getFormData: function (options) {
            var formData;
            return typeof options.formData == "function" ? options.formData(options.form) : $.isArray(options.formData) ? options.formData : options.formData ? (formData = [], $.each(options.formData, function (name, value) {
                formData.push({name: name, value: value})
            }), formData) : []
        }, _getTotal: function (files) {
            var total = 0;
            return $.each(files, function (index, file) {
                total += file.size || 1
            }), total
        }, _onProgress: function (e, data) {
            if (e.lengthComputable) {
                var total = data.total || this._getTotal(data.files), loaded = parseInt(e.loaded / e.total * (data.chunkSize || total), 10) + (data.uploadedBytes || 0);
                this._loaded += loaded - (data.loaded || data.uploadedBytes || 0), data.lengthComputable = !0, data.loaded = loaded, data.total = total, this._trigger("progress", e, data), this._trigger("progressall", e, {lengthComputable: !0, loaded: this._loaded, total: this._total})
            }
        }, _initProgressListener: function (options) {
            var that = this, xhr = options.xhr ? options.xhr() : $.ajaxSettings.xhr();
            xhr.upload && xhr.upload.addEventListener && (xhr.upload.addEventListener("progress", function (e) {
                that._onProgress(e, options)
            }, !1), options.xhr = function () {
                return xhr
            })
        }, _initXHRData: function (options) {
            var formData, file = options.files[0];
            if (!options.multipart || options.blob)options.headers = $.extend(options.headers, {"X-File-Name": file.name, "X-File-Type": file.type, "X-File-Size": file.size}), options.blob ? options.multipart || (options.contentType = "application/octet-stream", options.data = options.blob) : (options.contentType = file.type, options.data = file);
            options.multipart && typeof FormData != "undefined" && (options.formData instanceof FormData ? formData = options.formData : (formData = new FormData, $.each(this._getFormData(options), function (index, field) {
                formData.append(field.name, field.value)
            })), options.blob ? formData.append(options.paramName, options.blob) : $.each(options.files, function (index, file) {
                file instanceof Blob && formData.append(options.paramName, file)
            }), options.data = formData), options.blob = null
        }, _initIframeSettings: function (options) {
            options.dataType = "iframe " + (options.dataType || ""), options.formData = this._getFormData(options)
        }, _initDataSettings: function (options) {
            this._isXHRUpload(options) ? this._chunkedUpload(options, !0) || (options.data || this._initXHRData(options), this._initProgressListener(options)) : this._initIframeSettings(options)
        }, _initFormSettings: function (options) {
            if (!options.form || !options.form.length)options.form = $(options.fileInput.prop("form"));
            options.paramName || (options.paramName = options.fileInput.prop("name") || "files[]"), options.url || (options.url = options.form.prop("action") || location.href), options.type = (options.type || options.form.prop("method") || "").toUpperCase(), options.type !== "POST" && options.type !== "PUT" && (options.type = "POST")
        }, _getAJAXSettings: function (data) {
            var options = $.extend({}, this.options, data);
            return this._initFormSettings(options), this._initDataSettings(options), options
        }, _enhancePromise: function (promise) {
            return promise.success = promise.done, promise.error = promise.fail, promise.complete = promise.always, promise
        }, _getXHRPromise: function (resolveOrReject, context, args) {
            var dfd = $.Deferred(), promise = dfd.promise();
            return context = context || this.options.context || promise, resolveOrReject === !0 ? dfd.resolveWith(context, args) : resolveOrReject === !1 && dfd.rejectWith(context, args), promise.abort = dfd.promise, this._enhancePromise(promise)
        }, _chunkedUpload: function (options, testOnly) {
            var that = this, file = options.files[0], fs = file.size, ub = options.uploadedBytes = options.uploadedBytes || 0, mcs = options.maxChunkSize || fs, slice = file.webkitSlice || file.mozSlice || file.slice, upload, n, jqXHR, pipe;
            return!(this._isXHRUpload(options) && slice && (ub || mcs < fs)) || options.data ? !1 : testOnly ? !0 : ub >= fs ? (file.error = "uploadedBytes", this._getXHRPromise(!1)) : (n = Math.ceil((fs - ub) / mcs), upload = function (i) {
                return i ? upload(i -= 1).pipe(function () {
                    var o = $.extend({}, options);
                    return o.blob = slice.call(file, ub + i * mcs, ub + (i + 1) * mcs), o.chunkSize = o.blob.size, that._initXHRData(o), that._initProgressListener(o), jqXHR = ($.ajax(o) || that._getXHRPromise(!1, o.context)).done(function () {
                        o.loaded || that._onProgress($.Event("progress", {lengthComputable: !0, loaded: o.chunkSize, total: o.chunkSize}), o), options.uploadedBytes = o.uploadedBytes += o.chunkSize
                    }), jqXHR
                }) : that._getXHRPromise(!0)
            }, pipe = upload(n), pipe.abort = function () {
                return jqXHR.abort()
            }, this._enhancePromise(pipe))
        }, _beforeSend: function (e, data) {
            this._active === 0 && this._trigger("start"), this._active += 1, this._loaded += data.uploadedBytes || 0, this._total += this._getTotal(data.files)
        }, _onDone: function (result, textStatus, jqXHR, options) {
            this._isXHRUpload(options) || this._onProgress($.Event("progress", {lengthComputable: !0, loaded: 1, total: 1}), options), options.result = result, options.textStatus = textStatus, options.jqXHR = jqXHR, this._trigger("done", null, options)
        }, _onFail: function (jqXHR, textStatus, errorThrown, options) {
            options.jqXHR = jqXHR, options.textStatus = textStatus, options.errorThrown = errorThrown, this._trigger("fail", null, options), options.recalculateProgress && (this._loaded -= options.loaded || options.uploadedBytes || 0, this._total -= options.total || this._getTotal(options.files))
        }, _onAlways: function (result, textStatus, jqXHR, errorThrown, options) {
            this._active -= 1, options.result = result, options.textStatus = textStatus, options.jqXHR = jqXHR, options.errorThrown = errorThrown, this._trigger("always", null, options), this._active === 0 && (this._trigger("stop"), this._loaded = this._total = 0)
        }, _onSend: function (e, data) {
            var that = this, jqXHR, slot, pipe, options = that._getAJAXSettings(data), send = function (resolve, args) {
                return that._sending += 1, jqXHR = jqXHR || (resolve !== !1 && that._trigger("send", e, options) !== !1 && (that._chunkedUpload(options) || $.ajax(options)) || that._getXHRPromise(!1, options.context, args)).done(function (result, textStatus, jqXHR) {
                    that._onDone(result, textStatus, jqXHR, options)
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    that._onFail(jqXHR, textStatus, errorThrown, options)
                }).always(function (a1, a2, a3) {
                    that._sending -= 1, a3 && a3.done ? that._onAlways(a1, a2, a3, undefined, options) : that._onAlways(undefined, a2, a1, a3, options);
                    if (options.limitConcurrentUploads && options.limitConcurrentUploads > that._sending) {
                        var nextSlot = that._slots.shift();
                        while (nextSlot) {
                            if (!nextSlot.isRejected()) {
                                nextSlot.resolve();
                                break
                            }
                            nextSlot = that._slots.shift()
                        }
                    }
                }), jqXHR
            };
            return this._beforeSend(e, options), this.options.sequentialUploads || this.options.limitConcurrentUploads && this.options.limitConcurrentUploads <= this._sending ? (this.options.limitConcurrentUploads > 1 ? (slot = $.Deferred(), this._slots.push(slot), pipe = slot.pipe(send)) : pipe = this._sequence = this._sequence.pipe(send, send), pipe.abort = function () {
                var args = [undefined, "abort", "abort"];
                return jqXHR ? jqXHR.abort() : (slot && slot.rejectWith(args), send(!1, args))
            }, this._enhancePromise(pipe)) : send()
        }, _onAdd: function (e, data) {
            var that = this, result = !0, options = $.extend({}, this.options, data), fileSet = data.files, limit = options.limitMultiFileUploads, i;
            if (!options.singleFileUploads && !limit || !this._isXHRUpload(options))fileSet = [fileSet]; else if (!options.singleFileUploads && limit) {
                fileSet = [];
                for (i = 0; i < data.files.length; i += limit)fileSet.push(data.files.slice(i, i + limit))
            }
            return $.each(fileSet, function (index, file) {
                var files = $.isArray(file) ? file : [file], newData = $.extend({}, data, {files: files});
                return newData.submit = function () {
                    return that._onSend(e, newData)
                }, result = that._trigger("add", e, newData)
            }), result
        }, _normalizeFile: function (index, file) {
            file.name === undefined && file.size === undefined && (file.name = file.fileName, file.size = file.fileSize)
        }, _replaceFileInput: function (input) {
            var inputClone = input.clone(!0);
            $("<form></form>").append(inputClone)[0].reset(), input.after(inputClone).detach(), this.options.fileInput = this.options.fileInput.map(function (i, el) {
                return el === input[0] ? inputClone[0] : el
            })
        }, _onChange: function (e) {
            var that = e.data.fileupload, data = {files: $.each($.makeArray(e.target.files), that._normalizeFile), fileInput: $(e.target), form: $(e.target.form)};
            data.files.length || (data.files = [
                {name: e.target.value.replace(/^.*\\/, "")}
            ]), data.form.length ? data.fileInput.data("blueimp.fileupload.form", data.form) : data.form = data.fileInput.data("blueimp.fileupload.form"), that.options.replaceFileInput && that._replaceFileInput(data.fileInput);
            if (that._trigger("change", e, data) === !1 || that._onAdd(e, data) === !1)return!1
        }, _onDrop: function (e) {
            var that = e.data.fileupload, dataTransfer = e.dataTransfer = e.originalEvent.dataTransfer, data = {files: $.each($.makeArray(dataTransfer && dataTransfer.files), that._normalizeFile)};
            if (that._trigger("drop", e, data) === !1 || that._onAdd(e, data) === !1)return!1;
            e.preventDefault()
        }, _onDragOver: function (e) {
            var that = e.data.fileupload, dataTransfer = e.dataTransfer = e.originalEvent.dataTransfer;
            if (that._trigger("dragover", e) === !1)return!1;
            dataTransfer && (dataTransfer.dropEffect = dataTransfer.effectAllowed = "copy"), e.preventDefault()
        }, _initEventHandlers: function () {
            var ns = this.options.namespace || this.name;
            this.options.dropZone.bind("dragover." + ns, {fileupload: this}, this._onDragOver).bind("drop." + ns, {fileupload: this}, this._onDrop), this.options.fileInput.bind("change." + ns, {fileupload: this}, this._onChange)
        }, _destroyEventHandlers: function () {
            var ns = this.options.namespace || this.name;
            this.options.dropZone.unbind("dragover." + ns, this._onDragOver).unbind("drop." + ns, this._onDrop), this.options.fileInput.unbind("change." + ns, this._onChange)
        }, _beforeSetOption: function (key, value) {
            this._destroyEventHandlers()
        }, _afterSetOption: function (key, value) {
            var options = this.options;
            options.fileInput || (options.fileInput = $()), options.dropZone || (options.dropZone = $()), this._initEventHandlers()
        }, _setOption: function (key, value) {
            var refresh = $.inArray(key, this._refreshOptionsList) !== -1;
            refresh && this._beforeSetOption(key, value), $.Widget.prototype._setOption.call(this, key, value), refresh && this._afterSetOption(key, value)
        }, _create: function () {
            var options = this.options;
            options.fileInput === undefined ? options.fileInput = this.element.is("input:file") ? this.element : this.element.find("input:file") : options.fileInput || (options.fileInput = $()), options.dropZone || (options.dropZone = $()), this._slots = [], this._sequence = this._getXHRPromise(!0), this._sending = this._active = this._loaded = this._total = 0, this._initEventHandlers()
        }, destroy: function () {
            this._destroyEventHandlers(), $.Widget.prototype.destroy.call(this)
        }, enable: function () {
            $.Widget.prototype.enable.call(this), this._initEventHandlers()
        }, disable: function () {
            this._destroyEventHandlers(), $.Widget.prototype.disable.call(this)
        }, add: function (data) {
            if (!data || this.options.disabled)return;
            data.files = $.each($.makeArray(data.files), this._normalizeFile), this._onAdd(null, data)
        }, send: function (data) {
            if (data && !this.options.disabled) {
                data.files = $.each($.makeArray(data.files), this._normalizeFile);
                if (data.files.length)return this._onSend(null, data)
            }
            return this._getXHRPromise(!1, data && data.context)
        }})
    }(jQuery), function ($) {
        "use strict";
        function extension(name) {
            var components = name.split(".");
            return components.length ? components[components.length - 1] : null
        }

        var images = {ipa: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAADHmlDQ1BJQ0MgUHJvZmlsZQAAeAGFVN9r01AU/tplnbDhizpnEQk+aJFuZFN0Q5y2a1e6zVrqNrchSJumbVyaxiTtfrAH2YtvOsV38Qc++QcM2YNve5INxhRh+KyIIkz2IrOemzRNJ1MDufe73/nuOSfn5F6g+XFa0xQvDxRVU0/FwvzE5BTf8gFeHEMr/GhNi4YWSiZHQA/Tsnnvs/MOHsZsdO5v36v+Y9WalQwR8BwgvpQ1xCLhWaBpXNR0E+DWie+dMTXCzUxzWKcECR9nOG9jgeGMjSOWZjQ1QJoJwgfFQjpLuEA4mGng8w3YzoEU5CcmqZIuizyrRVIv5WRFsgz28B9zg/JfsKiU6Zut5xCNbZoZTtF8it4fOX1wjOYA1cE/Xxi9QbidcFg246M1fkLNJK4RJr3n7nRpmO1lmpdZKRIlHCS8YlSuM2xp5gsDiZrm0+30UJKwnzS/NDNZ8+PtUJUE6zHF9fZLRvS6vdfbkZMH4zU+pynWf0D+vff1corleZLw67QejdX0W5I6Vtvb5M2mI8PEd1E/A0hCgo4cZCjgkUIMYZpjxKr4TBYZIkqk0ml0VHmyONY7KJOW7RxHeMlfDrheFvVbsrj24Pue3SXXjrwVhcW3o9hR7bWB6bqyE5obf3VhpaNu4Te55ZsbbasLCFH+iuWxSF5lyk+CUdd1NuaQU5f8dQvPMpTuJXYSWAy6rPBe+CpsCk+FF8KXv9TIzt6tEcuAcSw+q55TzcbsJdJM0utkuL+K9ULGGPmQMUNanb4kTZyKOfLaUAsnBneC6+biXC/XB567zF3h+rkIrS5yI47CF/VFfCHwvjO+Pl+3b4hhp9u+02TrozFa67vTkbqisXqUj9sn9j2OqhMZsrG+sX5WCCu0omNqSrN0TwADJW1Ol/MFk+8RhAt8iK4tiY+rYleQTysKb5kMXpcMSa9I2S6wO4/tA7ZT1l3maV9zOfMqcOkb/cPrLjdVBl4ZwNFzLhegM3XkCbB8XizrFdsfPJ63gJE722OtPW1huos+VqvbdC5bHgG7D6vVn8+q1d3n5H8LeKP8BqkjCtbCoV8yAAAACXBIWXMAAAsTAAALEwEAmpwYAAAGf0lEQVRYCb1XW0yURxT+9splAQUFIvflUooIBSSCkVQbUWhqLCSSNkFfbKNN+9CqfWmMKfFBHyQ1farhpTYmVIsP0EuCd7RQgYq0QCwQ7oiwsJW7y+7Cbs8ZHPqzu2Dp7ZBh5p85c8435zazKqfTCUkqIqPRmFJY+GYxoFY7HA65BB7Lb9kvLCzAarVqbTabzmq3zptHzT+UlJTc2bVr1/zSxhcMVEoAzLt79+73qqoqvwBUcDqc4D9WyHysUAKRY7vdDovFgunpafT09EzevXv3fE5OTumhQ4dmX6BbLGtdmUiBdmJiis6vwoJjgWAQEFIuQbgC4LX5+XnRAgIC1iUkJHys0WiyKioqCoqKimyu8l2/1a4T/M3KyRtQq9SLvVoNEkrziz2PlU2n08FgMIh1AuGXlJSU7+Pjc6+srGyTJ/nKOY8AmIEBKJtUzr0cK0ExCH9/f2EpWldt3749Kygo6BuyhEap0HXsBkAqcGXkb2EVFytotdol63h7e4PbzMwMNmzYgNzc3BySV33p0iWDJ3k85waAJ6UiCUb2rvNKNzAQbnq9Hh0dHWhubkZfXx/i4uJyyRINlZWVMSzbldyCkBmUpl+2gdwyMjwMk8kEs9mMyclJsexH/l+3fr1wwXrqW1tbQacW7vDz8+MANT5+/PjY7OzsqeLi4imlzBUAcPAtApHMtBmddLLJqSlERUUhOTkZrIxpfHwcAwMD6O3pgbePD3bu3CmUsysYKAHwpezJo6CsIgC3pUzuPQKQWSAZWRCblE+zJTUdD1o6UVHdiK6+IcESHxOOVzbHYWtmFvp7u9De3o6UlBQEBgYiPDwcVKgQHR2dWF1dnUYb/gqAxfRj6ZznbW1toLSC3hCILyuuwzSjQuKWrTiY/xa8vTSYnTBj5qkJd+rbkPJSJAKpOHEcZGZmCnfyXnYrxYdeIFb8W8ECiwBYOZt2dHSUTp6B8m/vYcRqwN7X8+HlmEbr/WsY7u/G+NgIzMNDOPjRKVTeaMDbb+xAc1ODiBW2ABcv1k2palXoFkOPAGQQcvR3d3cjIiICjS1deNA9CWNWNqWaFp8f+8BVFnyDo9DUX4P4th7ExcSgv79PxAvLYaLe7Y5wS0OZcrLv7e3Fxo0b0dDSA4tvGAbI/M+cWlH/bTY7giLikbBtt/h+5tBgLiAK9a29CA4OJusNitSUKUpp++fN9xy+RwuwchWVYabBwUFRZpvahzATnYRJmxfsGj3mLHM4/nU9TDYdNnkvoLH6KuzQYEwTipmOn0XADg0NiSL1XNeysZxbBQBfQipxMr5sLAtaPLHooJrTY16tg3lsDA4vPUx059mftMM0MkIA1BicViFyXiMuJ74luVitRh4BKGOAqpjI5cTYCASH+CInJwzjA4/goAA1eAFtn72LlppqqMhqBm891At2JBrDMUYAea9aQ5Ykw3NAs2VdaVUAzBxDwdTV1YWMl8OhCw3A/bLj6PqlgYQHwl/nhMU8LMYxm9MwMkOFZfYpMlLCxR7eq1EvWoABeCKPACQjWyI1NRVXrlzB3n1FsGrV+GluWhSYpG070DnuwKcXr8I00Idp/3hcr3uE1yLs2JEcjmvf/4gDBw6IU7OcNQGQLuCei0ljYyNam2qR9do+fHL6NIYnZjEcEIubJhsutDuwye6Hzt9oXfM73slPIt6bXPmQkZEhzsJyPJmfF92dQpMSgNhN/woLC8XFU3ejEs65WYxMWHDjdg26aqqg/fU7BDxpwvuJThzfa8SjhmpxNxQUFCzJYXny7SBlyv6FLmDGsLAwHD16FJcvX8bFC+eRnZ2NklczEBkZKeRwqj58+BBfldWDXkM4cuSI2CPNvpoF3ABw2ZTEG6UQLqknTpxAXV0damtrUVpayo9QwRobG4u0tDQcPnwY9BLioOewJ1Nyv2hRKUdsUPxzA8Brq+UuX7Xc1kp8GE/kBmCxCnpmLi8vF+avr68Hl2h+iO7fvx9sgVu3boHnT5486UnPinMeg3AlblbKDxPuWXFISIi4qpmfAfAag1gLrQmAUjArYqVGo1Fc19zTjxrxHFPyKcfK+JLzfxtAaGgozp49K14+0iXc/28W4BOzC5j4EXrmzBnhf36KrQkEp4ey5eXlfUjf/wnt2bOHZS/TtywLKFVU6enpo+fOnZMu+ld7eiGPsg4+nRS87NcxrXFMsF1jqPGvmSVGGv8T4rzmX8t91EZJ/1K1+wOHAy2rSxoOAAAAAABJRU5ErkJggg==", zip: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAADHmlDQ1BJQ0MgUHJvZmlsZQAAeAGFVN9r01AU/tplnbDhizpnEQk+aJFuZFN0Q5y2a1e6zVrqNrchSJumbVyaxiTtfrAH2YtvOsV38Qc++QcM2YNve5INxhRh+KyIIkz2IrOemzRNJ1MDufe73/nuOSfn5F6g+XFa0xQvDxRVU0/FwvzE5BTf8gFeHEMr/GhNi4YWSiZHQA/Tsnnvs/MOHsZsdO5v36v+Y9WalQwR8BwgvpQ1xCLhWaBpXNR0E+DWie+dMTXCzUxzWKcECR9nOG9jgeGMjSOWZjQ1QJoJwgfFQjpLuEA4mGng8w3YzoEU5CcmqZIuizyrRVIv5WRFsgz28B9zg/JfsKiU6Zut5xCNbZoZTtF8it4fOX1wjOYA1cE/Xxi9QbidcFg246M1fkLNJK4RJr3n7nRpmO1lmpdZKRIlHCS8YlSuM2xp5gsDiZrm0+30UJKwnzS/NDNZ8+PtUJUE6zHF9fZLRvS6vdfbkZMH4zU+pynWf0D+vff1corleZLw67QejdX0W5I6Vtvb5M2mI8PEd1E/A0hCgo4cZCjgkUIMYZpjxKr4TBYZIkqk0ml0VHmyONY7KJOW7RxHeMlfDrheFvVbsrj24Pue3SXXjrwVhcW3o9hR7bWB6bqyE5obf3VhpaNu4Te55ZsbbasLCFH+iuWxSF5lyk+CUdd1NuaQU5f8dQvPMpTuJXYSWAy6rPBe+CpsCk+FF8KXv9TIzt6tEcuAcSw+q55TzcbsJdJM0utkuL+K9ULGGPmQMUNanb4kTZyKOfLaUAsnBneC6+biXC/XB567zF3h+rkIrS5yI47CF/VFfCHwvjO+Pl+3b4hhp9u+02TrozFa67vTkbqisXqUj9sn9j2OqhMZsrG+sX5WCCu0omNqSrN0TwADJW1Ol/MFk+8RhAt8iK4tiY+rYleQTysKb5kMXpcMSa9I2S6wO4/tA7ZT1l3maV9zOfMqcOkb/cPrLjdVBl4ZwNFzLhegM3XkCbB8XizrFdsfPJ63gJE722OtPW1huos+VqvbdC5bHgG7D6vVn8+q1d3n5H8LeKP8BqkjCtbCoV8yAAAACXBIWXMAAAsTAAALEwEAmpwYAAAECElEQVRYCaVXS0ibQRCePH2iiIjiOwp6EA+CingQi6DQg1jwFQ+eKl4S2kMvgiCtFtFDoaVHQQiiVTH4wJNGr4Kg0IOCiHdFEUEJhjR2vi0b/iS7/kk6sNnd2Xl8M//M/28sLy8vZCSLxfKW9+94OIz8+HVPT4/78vLSBn5TU1Pk9vbW9/T09PP4+Pg324zEy+v2lngAlZWV/tPTUwDQUmZmJs3Pz1N5eTllZGTQxcUFjY6O0traWvjg4ODL4eHh12RB2OO9WK1WEfn9/X38UXSfl5dH4XCYTk5OaGhoiM7Ozoj1qLOz084BfH5+fi7iTH5MBoQ1atWwYGUyGxAfHh6m3d1d4dzhcBCyWVZWZpmbm/N2dHR8N5jULrUAEJFuAByc+f1+crvdwrjT6aScnBwKhULU3NxMMzMzHs7ID5ZV+pCItIdmGcB5X18fra+vi2zZbDbCiEQihBppaWmh6elpb1dX1zfpTDUrAcC4LnrwcY4M7Ozs0MDAgFiDL0HAEUC0trbS1NTUh9cykVCEUJbRY60inIOQgY2NDbEGDwAwcw3Q3d2d2BcXF6NDvHwW5rNP8YWZdgbgdXNzkwYHB6MAUIjokOrqaqqrqyPuCNGmwWCQGhoavPn5+W+EsOFHmwGkVEeIEo+gv7+fVldXo48AhVhQUEB2u10Uo9RHyxYVFdl9Pt975gUkH3MCAPmMzQCg2FCA4+PjtLi4GG3F3NxcysrKEsUIkCDMLpeLHh4eMgTD8JMAAGeI0AwA5PASWlpawlLoIHLUAUg6l2u0KNMf/BhJmWcJACB0Aw5QgHgZGeVlJwCMHKgNrFWk5EqDKgXwcA4A3d3dtLKyItbJ6KjspZUBCeDo6Ig8Hk8MAF3GoKMibQZ0CjCCs/Pzc7q5uRGFWFVVJXiv6aicg5d2BtDne3t7xJ9eqq+vj6kDVRZ04LQZgBEdwVh2dra4B0CmsLAwCuA1HdVZWgDwDkDUuJCAamtrRd+bgU4aAAR1KZNGSkpKqLS0VGyxNpPXnaeVAXitqKighYUFQo/jq/da9JBPGYBOAcZANTU1xBdRkXoXv2bN5P9pJf6mnQFE3N7eLiwm41wnowQAqzoFYwzJyEh5nawSgE54e3ub9vf3hU1cNPi6RYFAgMbGxmhiYkLwwRsZGZF+TWd9sytUYXxyclKcNDY2Ev8Roevra3p8fBQ8AAGgVCglAPikbm1tEaLv7e1N8IMMAVgqpHwEOgP4+GDMzs4qRZAhjFQoJQDLy8vCNt/5RRba2tpifKXqXCjju24c3N9+3iuJn/ULP3MxsAbJGXwzYoe/WCbGX0IGrq6u/NwFuD9lxYT3/5sgm9iNN5Pw75idA5STRwK4eOUU92GWD3EGMEfpL/0rpUClqGEIAAAAAElFTkSuQmCC", unknown: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAADHmlDQ1BJQ0MgUHJvZmlsZQAAeAGFVN9r01AU/tplnbDhizpnEQk+aJFuZFN0Q5y2a1e6zVrqNrchSJumbVyaxiTtfrAH2YtvOsV38Qc++QcM2YNve5INxhRh+KyIIkz2IrOemzRNJ1MDufe73/nuOSfn5F6g+XFa0xQvDxRVU0/FwvzE5BTf8gFeHEMr/GhNi4YWSiZHQA/Tsnnvs/MOHsZsdO5v36v+Y9WalQwR8BwgvpQ1xCLhWaBpXNR0E+DWie+dMTXCzUxzWKcECR9nOG9jgeGMjSOWZjQ1QJoJwgfFQjpLuEA4mGng8w3YzoEU5CcmqZIuizyrRVIv5WRFsgz28B9zg/JfsKiU6Zut5xCNbZoZTtF8it4fOX1wjOYA1cE/Xxi9QbidcFg246M1fkLNJK4RJr3n7nRpmO1lmpdZKRIlHCS8YlSuM2xp5gsDiZrm0+30UJKwnzS/NDNZ8+PtUJUE6zHF9fZLRvS6vdfbkZMH4zU+pynWf0D+vff1corleZLw67QejdX0W5I6Vtvb5M2mI8PEd1E/A0hCgo4cZCjgkUIMYZpjxKr4TBYZIkqk0ml0VHmyONY7KJOW7RxHeMlfDrheFvVbsrj24Pue3SXXjrwVhcW3o9hR7bWB6bqyE5obf3VhpaNu4Te55ZsbbasLCFH+iuWxSF5lyk+CUdd1NuaQU5f8dQvPMpTuJXYSWAy6rPBe+CpsCk+FF8KXv9TIzt6tEcuAcSw+q55TzcbsJdJM0utkuL+K9ULGGPmQMUNanb4kTZyKOfLaUAsnBneC6+biXC/XB567zF3h+rkIrS5yI47CF/VFfCHwvjO+Pl+3b4hhp9u+02TrozFa67vTkbqisXqUj9sn9j2OqhMZsrG+sX5WCCu0omNqSrN0TwADJW1Ol/MFk+8RhAt8iK4tiY+rYleQTysKb5kMXpcMSa9I2S6wO4/tA7ZT1l3maV9zOfMqcOkb/cPrLjdVBl4ZwNFzLhegM3XkCbB8XizrFdsfPJ63gJE722OtPW1huos+VqvbdC5bHgG7D6vVn8+q1d3n5H8LeKP8BqkjCtbCoV8yAAAACXBIWXMAAAsTAAALEwEAmpwYAAACgElEQVRYCdWWy4oaURCGT1/iPeIqA4LSK3d5nRDcZ+0bSBAXGYOgj5FFskheII8yGQQ1gncd76b+M/bQ01Y71WIyk4JDdx//rvPVX+c0Gvv9XnnDMIy39Ox454T3Y9LdUL5fQr2W2YzY6Xa735n5R1MA3+12arPZqOl0qiaTye9Go1GlAn7QbzePxCceOAAT+vEYBQUHADC2260G6PV6b2q12ufVavWKIL5JITgAJFAYpwK/wwHLslQkElGLxQLQ0WazeY15KQQHYEoAAGeapoYAQDweV61WSxUKBbter1+TM5TG+PqUExyALhwQkgCEbdsqlUqpdrutobLZrE3t+IT3n4JgAbC4BAAa7ANEIpHQ+wFzgMrlcna1WoUTJyFYACSUAECHwIKxWExf8R4cAZjjOFalUnH3BNsOFgBJpADQYbFoNKoymYwql8t6L2Ajoi35fN4qFosf1+v1irRHR9RwLbyvRVf+rt/vf8HZDhPucex0Omo4HKr5fK6vs9lMAxJMt1Qqvaf1fnrzBjoAW8ME9Ol0WiWTSbVcLvVAEYPBQI1GI5yWK8qX8edkASCStsCbEN8EBNqB991vBFqBLybFvQB3h2ABUE1YB9yEWNg/sCmxJyiObGUBoDzHAbyHqv0AKOaQ7+jjwgJAfK4DgHAddBcGFFzggp11K+BekM55C8B9aABvAumifh0qx8ARfRYAALluhgY4bBp/UaGfzwa4RAu8tEH5/tom9C6O+9AAQS/4E0ufg1r6zxwIDfCsDsDWIGKp5VLdy23BpR0Iysc68CJaIO2hVPdfOXBLR/ADVRaXVifU3ZHu1q/l/hUnSPSaBq6XjDklm9C/Ylwf4g/yIOz68/LJtAAAAABJRU5ErkJggg=="};
        $.widget("testflight.filedrop", {options: {initialMessage: "Drop your file here, or click to select a file", inputName: "upload", inputId: "id_upload", uploadImmediately: !1, namespace: "filedrop", images: images, changed: function () {
        }}, _namespacedClass: function (name) {
            var options = this.options;
            return options.namespace + "-" + name
        }, _elementWithClass: function (type, name) {
            var element = $(document.createElement(type));
            return element.attr("class", this._namespacedClass(name)), element
        }, _showDropZone: function () {
            var options = this.options;
            options.message.html(this.options.initialMessage), options.progress.fadeOut("fast", function () {
                options.dropZone.fadeIn("fast")
            })
        }, _showProgressBar: function () {
            var options = this.options;
            this._setProgress(0), options.dropZone.fadeOut("fast", function () {
                options.progress.fadeIn("fast")
            })
        }, _reset: function () {
            this.options.data = null, this.options.input.val(""), this.options.dropDisabled = !1, this._showDropZone()
        }, _setProgress: function (percent) {
            percent = Math.floor(percent), this.options.progressBarInner.clearQueue(), percent < 101 && (this.options.progressBarInner.animate({width: percent + "%"}, 100), this._updateProgressBarMessage(percent + "% Uploaded..."))
        }, _updateMessage: function (name) {
            this.options.message.html('<img src="' + this._imageForName(name) + '" /> ' + name)
        }, _updateProgressBarMessage: function (name) {
            this.options.progressBarMessage.html(name)
        }, _imageForName: function (name) {
            var options = this.options;
            options.changed();
            var ext = extension(name);
            return options.images[ext] ? options.images[ext] : options.images.unknown
        }, _create: function () {
            var options = this.options;
            options.container = this._elementWithClass("div", "container"), options.dropZone = this._elementWithClass("div", "dropZone small"), options.message = this._elementWithClass("span", "message"), options.input = this._elementWithClass("input", "input"), options.input.attr("type", "file"), options.input.attr("name", options.inputName), options.input.attr("id", options.inputId), options.dropZone.append(options.message, options.input), options.progress = this._elementWithClass("div", "progress"), options.progressBar = this._elementWithClass("div", "progressBar");
            var progressBarInnerWrapper = $("<div></div>");
            options.progressBarInner = this._elementWithClass("div", "progressBarInner"), progressBarInnerWrapper.append(options.progressBarInner), options.progressBar.append(progressBarInnerWrapper), options.progressBarMessage = this._elementWithClass("span", "progressBarMessage"), this._updateProgressBarMessage("uploading..."), options.progress.append(options.progressBar, options.progressBarMessage), options.container.append(options.dropZone, options.progress), this.element.html(options.container);
            var _this = this;
            this.element.fileupload({dropZone: options.dropZone, add: function (e, data) {
                options.dropDisabled || (options.data = data, options.uploadImmediately ? _this.submit() : _this._updateMessage(data.files[0].name))
            }, done: function (e, data) {
                _this._setProgress(100)
            }, dataType: "json", replaceFileInput: !1}).bind("fileuploaddragover",function () {
                options.dropZone.addClass("active")
            }).bind("fileuploaddrop",function (e, data) {
                options.dropZone.removeClass("active"), _this._trigger("drop", e, data)
            }).bind("fileuploadprogress",function (e, data) {
                _this._setProgress(data.loaded / data.total * 100)
            }).bind("fileuploaddone",function (e, data) {
                setTimeout(function () {
                    _this._reset()
                }, 1e3), _this._trigger("done", null, data)
            }).bind("fileuploadfail", function (e, data) {
                setTimeout(function () {
                    _this._reset()
                }, 1e3), _this._trigger("fail", null, data)
            }), this._showDropZone()
        }, hasFile: function () {
            var options = this.options;
            return options.data !== null && options.data !== undefined
        }, getFile: function () {
            var options = this.options;
            return options.data
        }, submit: function () {
            var options = this.options;
            options.data && (options.dropDisabled = !0, this._showProgressBar(), this._setProgress(100), this._updateProgressBarMessage("Uploading..."), options.data.submit())
        }, destroy: function () {
            this.element.fileupload("destroy"), $.Widget.prototype.destroy.call(this)
        }})
    }(jQuery)
}).call(this);