window.onload = function () {
        function errorListFor(id) {
        var element = $('#' + id);
        var matches = element.prev('ul.errorlist');
        if (matches.length) {
            return $(matches[0]);
        } else {
            var errorlist = $('<ul class="errorlist error-message"></ul>');
            element.after(errorlist);
            $('.filedrop-dropZone').addClass('error');
            return errorlist;
        }
    }

    function displayError(id, message, url) {
        var element = $('#' + id);
        if (url) {
            element.after('<ul class="errorlist error-message"><li>' + message + ' <a style="text-decoration:underline" href="' + url + '">Learn More</a>.</li></ul>');
        }
        else {
            element.after('<ul class="errorlist error-message"><li>' + message + '</li></ul>');
        }
        $('.filedrop-dropZone').addClass('error');
    }

    var _gaq = _gaq || [];

    var tf_user_data = {
        id: undefined,
        email: undefined,
        type: 'Visistor'
    };

//    tf_user_data.id = 1737185;
//    tf_user_data.email = 'railshub@gmail.com';
//
//    tf_user_data.type = 'Developer';


    window.tf_wrap(function () {
        $(".tiptip").tipTip({maxWidth: "auto", defaultPosition: 'top', fadeIn: '400'});
        localise_times();

        $("a[data-team-id]").click(function () {
            var teamChangeForm = $("#team-change");
            $("#team", teamChangeForm).val($(this).data("teamId"));
            teamChangeForm.submit();
            return false
        });

        // dismiss global alert
        $('.global-alert .close-x').click(function () {
            var postURL = $(this).attr('href');
            //gobalID = '#'+$(this).attr('data-global-id');
            //$(gobalID).fadeOut();
            $(this).parent('.global-alert').fadeOut();
            $.post(postURL, function (data) {
                // post complete
            });
            return false;
        });

        // twispy
        $("[rel=twipsy]").twipsy({
            live: true,
            html: true
        });

        $('#tos_update').modal({
            backdrop: 'static',
            show: true
        });
    });

    function dump_obj(obj) {
        var output = '';
        for (property in obj) {
            output += property + ': ' + obj[property] + '; ';
        }
    }

    window.tf_wrap(function () {
        var dropped = false;

        $('#build-form').submit(function () {

            var haserror = false;
            $('#description').siblings('.errorlist').remove();
            $('#binary-filedrop').siblings('.errorlist').remove();

            var release_notes = $('#description').val();
            if (release_notes.length == 0) {
                displayError('description', 'This field is required.');
                $('#releasenotes').addClass('error');
                haserror = true;
            }
            if (release_notes.length > 0) {
                $('#releasenotes').removeClass('error');
            }

            var has_file = $('#binary-filedrop').filedrop('hasFile');
            if (!has_file) {
                displayError('binary-filedrop', 'This field is required.');
                haserror = true;
            }

            if (window.File && has_file) {
                $('#uploadbtn').hide();
                $('#uploadingbtn').show();
                var file = $('#binary-filedrop').filedrop('getFile');
                $('.filedrop-dropZone').removeClass('error');
                if (file.files) {
                    if (file.files[0].size > 838860800) {    // 800MB
                        displayError('binary-filedrop', "File is too big - Maximum size is 800MB. Contact <a href='mailto:support@goop.com'>support@goop.com</a> if you would like to upload a larger IPA or APK.")
                        haserror = true;
                    }
                }
            }

            if (haserror) {
                $('#uploadbtn').show();
                $('#uploadingbtn').hide();
                return false;
            }

            if (dropped) {
                _gaq.push(['_trackEvent', 'Build Upload', 'Dropped']);
            } else {
                _gaq.push(['_trackEvent', 'Build Upload', 'Selected']);
            }

            $('#binary-filedrop').filedrop('submit');
            return false;
        });

        $('#binary-filedrop')
            .filedrop({
                initialMessage: 'Drop your APK here or click to select a file',
                inputName: 'binary',
                inputId: 'id_binary'
            })
            .bind('filedropdrop', function (e, options) {
                dropped = true;
            })
            .bind('filedropdone', function (e, options) {
                var data = options.result;
                if (data.redirect) {
                    window.location = data.redirect;
                } else if (data.errors) {
                    if (data.errors.binary) {
                        displayError('binary-filedrop', data.errors.binary, data.help_url);
                        $('#uploadbtn').show();
                        $('#uploadingbtn').hide();
                    }
                }
//                else if (data.deprecated_android) {
//                    // Handle cancel click
//                    $('#deprecated_android_continuelink').attr('href', data.link);
//                    $('#deprecated_android_dialog').modal({
//                        backdrop: true,
//                        show: true
//                    });
//                    $('#uploadbtn').show();
//                    $('#uploadingbtn').hide();
//                }
                else if (data.deprecated_sdk) {
                    $('#deprecatedsdk_version').html(data.deprecated_sdk_version);
                    // Handle cancel click
                    $('#deprecatedsdk_cancelupload').click(function () {
                        if (data.build_id) {
                            window.location.replace('/dashboard/builds/reupload/' + data.build_id);
                        }
                    });
                    $('#deprecatedsdk_continuelink').attr('href', data.link);
                    $('#deprecatedsdk_dialog').modal({
                        backdrop: true,
                        show: true
                    });
                    $('#uploadbtn').show();
                    $('#uploadingbtn').hide();
                }
                else if (data.token_missing) {
                    if (data.team_token_found) {
                        $('.apptokentitle').html('Outdated use of Team Token detected.')
                        $('.apptokenmsg').html('Make sure you are using GoopTest SDK 2.2+ with this App Token: ')
                    }
                    else {
                        $('.apptokentitle').html('The App Token for this app was not detected.')
                        $('.apptokenmsg').html('Please update your build with this App Token: ')
                        // Handle cancel click
                        $('#cancelUpload').click(function () {
                            if (data.build_id) {
                                window.location.replace('/dashboard/builds/reupload/' + data.build_id);
                            }
                        });
                    }
                    $('.apptokentoken').html(data.token);
                    $('#apptokencontinuelink').attr('href', data.link);
                    $('#apptokendialog').modal({
                        backdrop: true,
                        show: true
                    });
                    $('#uploadbtn').show();
                    $('#uploadingbtn').hide();
                } else if (data.developer_build) {

                    $('.devprofmsg').html(data.message)
                    // Handle cancel click
                    $('#devprofcancelUpload').click(function () {
                        if (data.build_id) {
                            window.location.replace('/dashboard/builds/reupload/' + data.build_id);
                        }
                    });
                    $('#devprofcontinuelink').attr('href', data.link);
                    $('#devprofdialog').modal({
                        backdrop: true,
                        show: true
                    });
                    $('#uploadbtn').show();
                    $('#uploadingbtn').hide();

                } else {
                    // $.ajax({'type': 'POST', 'url': '/dashboard/builds/error/', 'data': {'error': dump_obj(e) + "\ndata: " + dump_obj(data) + "\nbrowser: " + navigator.appVersion}, 'async':false});
                    displayError('binary-filedrop', 'An unexpected error has occurred.');
                    $('#uploadbtn').show();
                    $('#uploadingbtn').hide();
                }
            })
            .bind('filedropfail', function (e, data) {
                console.log("failed");
                // $.ajax({'type': 'POST', 'url': '/dashboard/builds/error/', 'data': {'error': dump_obj(e) + "\ndata: " + dump_obj(data) + "\nbrowser: " + navigator.appVersion}, 'async':false});
                displayError('binary-filedrop', 'An unexpected error has occurred.');
                $('#uploadbtn').show();
                $('#uploadingbtn').hide();
            });
    });


}