//
//  Utils.m
//  crumbit
//
//      GOOP.io
//      Target Platform: IOS
//      CopyRight :: Bad Juju Games, Inc.


#import "Utils.h"
#import "Constants.h"

#import <QuartzCore/QuartzCore.h>
@implementation Utils

void ShowMessage(NSString *title, NSString *msg)
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg
												   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
	[alert show];
	[alert release];
}
BOOL SaveStringWithKey(NSString* s, NSString* key)
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:s forKey:key];
    return [defaults synchronize];
}

NSString* GetStringWithKey(NSString* key)
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:key];
}

+(void) ShowMessageWithTitle:(NSString*)title withMessage:(NSString*)msg
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}
NSString * stringByStrippingHTML (NSString *ss) {
    
    NSMutableString *html = [NSMutableString stringWithCapacity:[ss length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:ss];
    scanner.charactersToBeSkipped = NULL;
    NSString *tempText = nil;
    
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&tempText];
        
        if (tempText != nil)
            [html appendString:tempText];
        
        [scanner scanUpToString:@">" intoString:NULL];
        
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation] + 1];
        
        tempText = nil;
    }
    
    return html;
    
    NSRange r;
    NSString *s = [[ss copy] autorelease];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}
+ (NSString*)encodeParamStringWithParameters:(NSDictionary*)parameters
{
    NSMutableString *parameterString = [[NSMutableString alloc]init];
    NSEnumerator *enumerator = [parameters keyEnumerator];
    NSString* key;
    BOOL firstParam = YES;
    while ((key = [enumerator nextObject])) 
    {
        if (!firstParam)
            [parameterString appendString:@"&"];
        
        firstParam = NO;
        
        id value = [parameters objectForKey:key];
        if ([value isKindOfClass:[NSString class]]) {
            value = [value URLEncode];
            [parameterString appendString:[NSString stringWithFormat:@"%@=%@", key, value]];
        }
        
    }
    
    NSString *tParametersString = [NSString stringWithString:parameterString];
    [parameterString release];
    
    return tParametersString;
}

+(BOOL)valideUserName:(NSString *)username
{
    NSString *myRegex = @"[A-Z0-9a-z_]*"; 
    NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", myRegex]; 
    return [myTest evaluateWithObject:username];
}

+ (BOOL) validateEmail: (NSString *)candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
    
    return [emailTest evaluateWithObject:candidate];
}
+ (BOOL) validateUrl: (NSString *) candidate {
    
    NSURL *myURL;
    if ([candidate hasPrefix:@"http://"]) {
        myURL = [NSURL URLWithString:candidate];
    } else {
        myURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",candidate]];
    }
    if (myURL && myURL.scheme && myURL.host) {
        return YES;
    }
    return NO;
}
+ (void)setPadding:(CGFloat)spacing  toButton:(UIButton*)v
{
    UIEdgeInsets inset = UIEdgeInsetsMake(spacing, spacing, spacing, spacing);
    [v setTitleEdgeInsets:inset];
}
+ (NSString *) getDocumentPathWithAppendinPath:(NSString *)string
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    return [documentsDir stringByAppendingPathComponent:string];
}

@end
