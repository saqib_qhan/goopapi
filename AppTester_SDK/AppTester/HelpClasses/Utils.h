//
//  Utils.h
//  Mosque Guide
//
//      GOOP.io
//      Target Platform: IOS
//      CopyRight :: Bad Juju Games, Inc.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class User;

@interface Utils : NSObject

// Degrees to radians
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

BOOL SaveStringWithKey(NSString* s, NSString* key);

NSString* GetStringWithKey(NSString* key);
bool IsLoggedIn(void);
bool logOutCurrentUser(void);
void ShowMessage(NSString *title, NSString *msg);
NSString * stringByStrippingHTML (NSString *ss);

+ (NSString *) getDocumentPathWithAppendinPath:(NSString *)string;
+ (NSString*)encodeParamStringWithParameters:(NSDictionary*)parameters;

+(void) ShowMessageWithTitle:(NSString*)title withMessage:(NSString*)msg;
+(void) addTextFieldPadding:(UITextField*)textField;

@end
