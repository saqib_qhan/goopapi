//
//  LoginViewController.h
//  CLoneTestFlight
//
//      GOOP.io
//      Target Platform: IOS
//      CopyRight :: Bad Juju Games, Inc.


#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (retain, nonatomic) IBOutlet UITextField *textUserName;
@property (retain, nonatomic) IBOutlet UITextField *textPassword;
- (IBAction)login:(id)sender;

@end
