//
//  MyPageViewController.m
//  AppTester
//
//  Created by My Mac on 05/04/2014.
//  Copyright (c) 2014 Soft. All rights reserved.
//

#import "MyPageViewController.h"
#import "MyFlightLibrary.h"
@interface MyPageViewController ()

@end

@implementation MyPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)init
{
    self = [super initWithNibName:@"MyPageViewController" bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewDidUnload {
    [self setActionTPFour:nil];
    [super viewDidUnload];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionTPOne:(id)sender {
    [MyFlightLibrary passTestPoint:@"TestPointOne"];
}
- (IBAction)actionTPTwo:(id)sender {
    [MyFlightLibrary passTestPoint:@"TestPointTwo"];
}
- (IBAction)actionTPFour:(id)sender {
    [MyFlightLibrary passTestPoint:@"TestPointFour"];
}
- (IBAction)actionTPThree:(id)sender {
    [MyFlightLibrary passTestPoint:@"TestPointThree"];
}
- (IBAction)actionTPFive:(id)sender {
    [MyFlightLibrary passTestPoint:@"TestPointFive"];
}


- (void)dealloc {
    [_actionTPFour release];
    [super dealloc];
}
@end
