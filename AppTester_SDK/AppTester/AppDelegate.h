//
//  AppDelegate.h
//  CLoneTestFlight
//
//      GOOP.io
//      Target Platform: IOS
//      CopyRight :: Bad Juju Games, Inc.


#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UINavigationController *navigationController;
}
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic,retain) RootViewController * rootViewController;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
