//
//  RootViewController.h
//  CLoneTestFlight
//
//      GOOP.io
//      Target Platform: IOS
//      CopyRight :: Bad Juju Games, Inc.

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController
- (IBAction)goToTestPointPage:(id)sender;

@end
