//
//  RootViewController.m
//  CLoneTestFlight
//
//      GOOP.io
//      Target Platform: IOS
//      CopyRight :: Bad Juju Games, Inc.


#import "RootViewController.h"
#import "MyFlightLibrary.h"
#import "MyPageViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goToTestPointPage:(id)sender {
    MyPageViewController * myPageVC=[[MyPageViewController alloc]init];
    [self.navigationController pushViewController:myPageVC animated:YES];
     [myPageVC release];
}
@end
