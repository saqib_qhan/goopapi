//
//  Constants.h
//  CLoneTestFlight
//
//      GOOP.io
//      Target Platform: IOS
//      CopyRight :: Bad Juju Games, Inc.

#ifndef CLoneTestFlight_Constants_h
#define CLoneTestFlight_Constants_h

#define kUserName   @"user_name"
#define kPassword   @"password"
#define kIsLogin    @"is_login"

#endif
