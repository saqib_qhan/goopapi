//
//  LoginViewController.m
//  CLoneTestFlight
//
//      GOOP.io
//      Target Platform: IOS
//      CopyRight :: Bad Juju Games, Inc.


#import "LoginViewController.h"
#import "Constants.h"
#import "Utils.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_textUserName release];
    [_textPassword release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTextUserName:nil];
    [self setTextPassword:nil];
    [super viewDidUnload];
}
- (IBAction)login:(id)sender {
    SaveStringWithKey(_textUserName.text, kUserName);
    SaveStringWithKey(_textPassword.text, kPassword);
    SaveStringWithKey(@"YES", kIsLogin);
}
@end
