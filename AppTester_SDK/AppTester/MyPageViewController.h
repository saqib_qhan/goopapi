//
//  MyPageViewController.h
//  AppTester
//
//  Created by My Mac on 05/04/2014.
//  Copyright (c) 2014 Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPageViewController : UIViewController
- (IBAction)actionTPOne:(id)sender;
- (IBAction)actionTPTwo:(id)sender;
- (IBAction)actionTPThree:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *actionTPFour;
- (IBAction)actionTPFive:(id)sender;
- (IBAction)actionTPFour:(id)sender;

@end
