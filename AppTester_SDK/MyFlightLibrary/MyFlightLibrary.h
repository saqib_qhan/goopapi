//
//  MyFlightLibrary.h
//  MyFlightLibrary
//
//  GOOP.io
//  Target Platform: IOS
//    CopyRight :: Bad Juju Games, Inc.

#import <Foundation/Foundation.h>

@interface MyFlightLibrary : NSObject
/**
 * Add custom information like username etc
 */
+ (void)addCustomInfo:(NSString *)info forKey:(NSString*)key;
/**
 * Starts a Goop session using the App Token
 */
+ (void)startGoop:(NSString *)appToken;
/**
 * device ID to be used for identification of the device and user using the app
 */
+ (void)setDeviceID:(NSString*)deviceID;
/**
 * Record that the particular passpoint with specific identity "testpointName"
 */
+ (void)passTestPoint:(NSString *)testpointName;

@end
