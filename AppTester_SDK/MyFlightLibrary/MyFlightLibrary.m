//
//  MyFlightLibrary.m
//  MyFlightLibrary
//
//      GOOP.io
//      Target Platform: IOS
//      CopyRight :: Bad Juju Games, Inc.


#import "MyFlightLibrary.h"

@implementation MyFlightLibrary

+ (void)addCustomInfo:(NSString *)info forKey:(NSString*)key
{
    [self saveInfoToPlist:info WithKey:key];
    NSLog(@"Information: %@",info);
    NSLog(@"Key: %@",key);
}
+ (void)setDeviceID:(NSString*)deviceID{
    [self saveInfoToPlist:deviceID WithKey:deviceID];
     NSLog(@"device Identifier: %@",deviceID);
}
+ (void)startGoop:(NSString *)appToken{
    [self deleteAllInfoFromPlist];
    NSLog(@"App Token: %@",appToken);
}
+ (void)passTestPoint:(NSString *)testpointName{
    NSLog(@"TestPoint: %@",testpointName);
    [self saveInfoToPlist:testpointName WithKey:[self getCurrentDateAndTime]];
    [self nsLogFullInfoFromPlist];
}
+(void)saveInfoToPlist:(NSString *)info WithKey:(NSString *)key{
    
    //First of All get all the paths to any directory in domain
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //Get the path at first index as it stores the file of plist type
    NSString *documentsDirectory = [pathsArray objectAtIndex:0];
    //Now we have to maintain our plist named "AppTesterplist.plist" which will contain all the logs which will be printed and called by our library
    NSString *pathToPlist = [documentsDirectory stringByAppendingPathComponent:@"AppTesterplist.plist"];
    //After getting the path to that plist we will manage it through the file manager
    NSFileManager *myFileManager = [NSFileManager defaultManager];
    
    //This will called the first time when our plist is not created
    if (![myFileManager fileExistsAtPath: pathToPlist])
    {
        //it will creat a empty new plist with name of "Apptesterplist.plist" and return the path
        pathToPlist = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"AppTesterplist.plist"] ];
    }
    
    NSMutableDictionary *dataInFile;
    
    if ([myFileManager fileExistsAtPath: pathToPlist])
    {
        //Now get all the data stored in our plist
        dataInFile = [[NSMutableDictionary alloc] initWithContentsOfFile: pathToPlist];
    }
    else
    {
        //For the first time when the plist is not created
        dataInFile = [[NSMutableDictionary alloc] init];
    }
    
    //Now add your data in that plist data and then resave all the data again to plist
    [dataInFile setObject:info forKey:key];
    [dataInFile writeToFile: pathToPlist atomically:YES];
    [dataInFile release];
    
}
+(void)nsLogFullInfoFromPlist{
    //First of All get all the paths to any directory in domain
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //Get the path at first index as it stores the file of plist type
    NSString *documentsDirectory = [pathsArray objectAtIndex:0];
     //Now we have to maintain our plist named "AppTesterplist.plist" which will contain all the logs which will be printed and called by our library
    NSString *pathToFile = [documentsDirectory stringByAppendingPathComponent:@"AppTesterplist.plist"];
    //After getting the path to that plist we will manage it through the file manager
    NSFileManager *myfileManager = [NSFileManager defaultManager];
    //This will called the first time when our plist is not created

    if (![myfileManager fileExistsAtPath: pathToFile])
    {
        NSLog(@"No plist is created till now");
        //return nill
    }
    
    NSMutableDictionary *data;
    
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: pathToFile];
    
    //To reterive the data from the plist
    NSMutableDictionary *savedData = [[NSMutableDictionary alloc] initWithContentsOfFile: pathToFile];
    
    NSLog(@"%@",savedData);
    [savedData release];
}
+(NSString *)getInfoForKeyFromPlist:(NSString*)key{
   
    //First of All get all the paths to any directory in domain
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //Get the path at first index as it stores the file of plist type
    NSString *documentsDirectory = [pathsArray objectAtIndex:0];
    //Now we have to maintain our plist named "AppTesterplist.plist" which will contain all the logs which will be printed and called by our library
    NSString *pathToFile = [documentsDirectory stringByAppendingPathComponent:@"AppTesterplist.plist"];
    //After getting the path to that plist we will manage it through the file manager
    NSFileManager *myfileManager = [NSFileManager defaultManager];
    //This will called the first time when our plist is not created
    
    if (![myfileManager fileExistsAtPath: pathToFile])
    {
        return nil;
    }
    
    NSMutableDictionary *data;
    
    data = [[NSMutableDictionary alloc] initWithContentsOfFile: pathToFile];
    
    //To reterive the data from the plist
    NSMutableDictionary *savedData = [[[NSMutableDictionary alloc] initWithContentsOfFile: pathToFile]autorelease];
    
    return [savedData objectForKey:key];
}
+(NSDictionary *)getAllDataFromPlist{
    
    //First of All get all the paths to any directory in domain
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //Get the path at first index as it stores the file of plist type
    NSString *documentsDirectory = [pathsArray objectAtIndex:0];
    //Now we have to maintain our plist named "AppTesterplist.plist" which will contain all the logs which will be printed and called by our library
    NSString *pathToFile = [documentsDirectory stringByAppendingPathComponent:@"AppTesterplist.plist"];
    //After getting the path to that plist we will manage it through the file manager
    NSFileManager *myfileManager = [NSFileManager defaultManager];
    //This will called the first time when our plist is not created
    
    if (![myfileManager fileExistsAtPath: pathToFile])
    {
        return nil;
    }
    
    NSMutableDictionary *data;
    
    data = [[[NSMutableDictionary alloc] initWithContentsOfFile: pathToFile]autorelease];
    
    //To reterive the data from the plist
    NSMutableDictionary *savedData = [[[NSMutableDictionary alloc] initWithContentsOfFile: pathToFile]autorelease];
    
    return savedData;
}
+(BOOL)sendToServerViaInternet:(NSString*)message WithKey:(NSString*)key
{
    if([self isInternetAvailable]){
        //send to Internet
        return TRUE;
    }
    else{
        return FALSE;
    }
}
+(BOOL)startAppTesterSessionWithAppToken:(NSString *)appToken
{
    //calling webservice for staring the session
    return FALSE;
}
+(BOOL)sendAllDataFromPlistToServerOnInternetAvailability{
    if ([self isInternetAvailable]) {
        //send all the data from plist
    }
    return FALSE;
}
+(BOOL)deleteAllInfoFromPlist{
    
    //First of All get all the paths to any directory in domain
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //Get the path at first index as it stores the file of plist type
    NSString *documentsDirectory = [pathsArray objectAtIndex:0];
    //Now we have to maintain our plist named "AppTesterplist.plist" which will contain all the logs which will be printed and called by our library
    NSString *pathToPlist = [documentsDirectory stringByAppendingPathComponent:@"AppTesterplist.plist"];
    //After getting the path to that plist we will manage it through the file manager
    NSFileManager *myFileManager = [NSFileManager defaultManager];
    
    //This will called the first time when our plist is not created
    if (![myFileManager fileExistsAtPath: pathToPlist])
    {
        //No AppTesterPlist then return true as empty plist
        return TRUE;
    }
    
    NSMutableDictionary *dataInFile;
    
    if ([myFileManager fileExistsAtPath: pathToPlist])
    {
        //Now get all the data stored in our plist
        dataInFile = [[NSMutableDictionary alloc] initWithContentsOfFile: pathToPlist];
    }
    else
    {
        //No AppTesterPlist then return true as empty plist
        return TRUE;
    }
    
    NSArray *keysArray = [dataInFile allKeys];
    
    for (int count = 0 ; count < [keysArray count]; count++)
    {
        [dataInFile removeObjectForKey:keysArray[count]];
    }
    
    [dataInFile writeToFile: pathToPlist atomically:YES];
    [dataInFile release];
    return TRUE;
    
}
+(BOOL)isInternetAvailable{
    //check for internet
    return NO;
}
+(NSString *)getCurrentDateAndTime{
    
    NSDate *currentDate = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeStyle:NSDateFormatterLongStyle];
    NSString *currentTime = [dateFormatter stringFromDate:currentDate];
    [dateFormatter release];
    NSLog(@"CurrentTime:%@",currentTime);
    return currentTime;
}
@end
